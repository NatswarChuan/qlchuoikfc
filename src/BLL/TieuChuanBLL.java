package BLL;

import DAO.TieuChuanDAO;
import DTO.TieuChuan;
import java.util.List;

public class TieuChuanBLL {
    private TieuChuanDAO tieuChuanDAO;

    public TieuChuanBLL() {
        tieuChuanDAO = new TieuChuanDAO();
    }
    
    public List<TieuChuan> getAllChungNhan() {
        return tieuChuanDAO.getAllChungNhan();
    }
    
    public TieuChuan getTieuChuanByMaTC(String maTC) {
        return tieuChuanDAO.getTieuChuanByMaTC(maTC);
    }
}
