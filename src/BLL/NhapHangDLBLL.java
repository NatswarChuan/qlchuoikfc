package BLL;

import DAO.NhapHangDaiLyDAO;
import DTO.NhapHangDaiLY;
import java.util.List;

public class NhapHangDLBLL {
    private NhapHangDaiLyDAO nhapHangNHDAO;

    public NhapHangDLBLL() {
        nhapHangNHDAO = new NhapHangDaiLyDAO();
    }
    
    public List<NhapHangDaiLY> getAllNhapHangNHs() {
        return nhapHangNHDAO.getAllNhapHangNHs();
    }
    
    public NhapHangDaiLY getNHNHByMaNH(String maPNNH) {
        return nhapHangNHDAO.getNHNHByMaNH(maPNNH);
    }
    
    public void insertNhapHangNH(NhapHangDaiLY nhapHangNH){
        nhapHangNHDAO.insertNhapHangNH(nhapHangNH);
    }
}
