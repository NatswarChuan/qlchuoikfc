package BLL;

import DAO.ChiTietHoaDonDAO;
import DTO.ChiTietHoaDon;
import java.util.List;

public class ChiTietHoaDonBLL {
    private ChiTietHoaDonDAO chiTietHoaDonDAO;

    public ChiTietHoaDonBLL() {
        chiTietHoaDonDAO = new ChiTietHoaDonDAO();
    }
    
    public List<ChiTietHoaDon> getAllChiTietHoaDon() {
        return chiTietHoaDonDAO.getAllChiTietHoaDon();
    }
    
    public List<ChiTietHoaDon> getCTHDByMaHD(String maHD) {
        return chiTietHoaDonDAO.getCTHDByMaHD(maHD);
    }
    
    public void insertCTHD(ChiTietHoaDon cthd){
        chiTietHoaDonDAO.insertCTHD(cthd);
    }
}
