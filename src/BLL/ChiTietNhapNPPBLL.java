package BLL;

import DAO.ChiTietNhapNPPDAO;
import DTO.ChiTietNhapNPP;
import java.util.List;

public class ChiTietNhapNPPBLL {
    private ChiTietNhapNPPDAO chiTietNhapNPPDAO;

    public ChiTietNhapNPPBLL() {
        chiTietNhapNPPDAO = new ChiTietNhapNPPDAO();
    }
    
    public List<ChiTietNhapNPP> getAllChiTietNhapNPP() {
        return chiTietNhapNPPDAO.getAllChiTietNhapNPP();
    }
    
    public List<ChiTietNhapNPP> getCTPNByMaPNPP(String maPNPP) {
        return chiTietNhapNPPDAO.getCTPNByMaPNPP(maPNPP);
    }
    
    public void insertCTPNNPP(ChiTietNhapNPP ctnnpp){
        chiTietNhapNPPDAO.insertCTPNNPP(ctnnpp);
    }
}
