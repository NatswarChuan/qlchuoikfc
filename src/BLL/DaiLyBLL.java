package BLL;

import DAO.DaiLyDAO;
import DTO.DaiLy;
import java.util.List;

public class DaiLyBLL {
    private DaiLyDAO nhaHangDAO;

    public DaiLyBLL() {
        nhaHangDAO = new DaiLyDAO();
    }
    
    public List<DaiLy> getAllNhaHang() {
        return nhaHangDAO.getAllNhaHang();
    }
    
    public DaiLy getNhaHangByMaNH(String maNH) {
        return  nhaHangDAO.getNhaHangByMaNH(maNH);
    }
    
    public void insertNhaHang(DaiLy nhaHang){
        nhaHangDAO.insertNhaHang(nhaHang);
    }
}
