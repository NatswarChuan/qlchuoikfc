package BLL;

import DAO.ChiTietNhapDaiLyDAO;
import DTO.ChiTietNhapDaiLy;
import java.util.List;

public class ChiTietNhapDLBLL {
    private ChiTietNhapDaiLyDAO chiTietNhapNHDAO;

    public ChiTietNhapDLBLL() {
        chiTietNhapNHDAO = new ChiTietNhapDaiLyDAO();
    }
    
    public List<ChiTietNhapDaiLy> getAllChiTietNhapNH() {
        return chiTietNhapNHDAO.getAllChiTietNhapNH();
    }
    
    public List<ChiTietNhapDaiLy> getCTPNByMaPNNH(String maPNNH) {
        return chiTietNhapNHDAO.getCTPNByMaPNNH(maPNNH);
    }
    
    public void insertCTPN(ChiTietNhapDaiLy ctnnh){
        chiTietNhapNHDAO.insertCTPN(ctnnh);
    }
}
