package BLL;

import DAO.NhapHangNPPDAO;
import DTO.NhapHangNPP;
import java.util.List;

public class NhapHangDLPBLL {
    private NhapHangNPPDAO nhapHangNPPDAO;

    public NhapHangDLPBLL() {
        nhapHangNPPDAO = new NhapHangNPPDAO();
    }
    
    public List<NhapHangNPP> getAllNhapHangNPP() {
        return nhapHangNPPDAO.getAllNhapHangNPP();
    }
    
    public NhapHangNPP getNHNPPByMaNPP(String maNPP) {
        return nhapHangNPPDAO.getNHNPPByMaNPP(maNPP);
    } 
    
    public void insertNhapHangNPP(NhapHangNPP nhapHangNPP){
        nhapHangNPPDAO.insertNhapHangNPP(nhapHangNPP);
    } 
    
}
