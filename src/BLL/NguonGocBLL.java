package BLL;

import DAO.NguonGocDAO;
import DTO.NguonGoc;
import java.util.List;

public class NguonGocBLL {
    
    private NguonGocDAO nguonGocDAO;

    public NguonGocBLL() {
        nguonGocDAO = new NguonGocDAO();
    }
    
    public List<NguonGoc> getAllNguonGoc() {
        return nguonGocDAO.getAllNguonGoc();
    }
    
    public List<NguonGoc> getNguonGocbyMaMA(String maMonAn) {
        return nguonGocDAO.getNguonGocbyMaMA(maMonAn);
    }
    
    public void insertNguonGoc(NguonGoc nguonGoc){
        nguonGocDAO.insertNguonGoc(nguonGoc);
    }
    
    public void deleteNguonGocByMaNL(String maNL){
        nguonGocDAO.deleteNguonGocByMaNL(maNL);
    }
    
     public void deleteNguonGocByMaMA(String maMa){
         nguonGocDAO.deleteNguonGocByMaMA(maMa);
    }
    
    public void deleteNguonGoc(String maMa, String maNL){
        nguonGocDAO.deleteNguonGoc(maMa, maNL);
    }

}
