package BLL;

import DAO.NhaPhanPhoiDAO;
import DTO.NhaPhanPhoi;
import java.util.List;

public class NhaPhanPhoiBLL {
    private NhaPhanPhoiDAO nhaPhanPhoiDAO;

    public NhaPhanPhoiBLL() {
        nhaPhanPhoiDAO = new NhaPhanPhoiDAO();
    }
    
    public List<NhaPhanPhoi> getAllNhaPhanPhoi() {
        return nhaPhanPhoiDAO.getAllNhaPhanPhoi();
    }
    
    public NhaPhanPhoi getNPPByMaNPP(String maNPP) {
        return nhaPhanPhoiDAO.getNPPByMaNPP(maNPP);
    }
    
    public void insertNhaPhanPhoi(NhaPhanPhoi nhaPhanPhoi){
        nhaPhanPhoiDAO.insertNhaPhanPhoi(nhaPhanPhoi);
    }
}
