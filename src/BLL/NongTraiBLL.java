package BLL;

import DAO.NongTraiDAO;
import DTO.NongTrai;
import java.util.List;

public class NongTraiBLL {
    private NongTraiDAO nongTraiDAO;

    public NongTraiBLL() {
        nongTraiDAO = new NongTraiDAO();
    }
    
    public List<NongTrai> getAllNongTrai() {
        return nongTraiDAO.getAllNongTrai();
    }
    
    public NongTrai getNongTraiByMaNT(String maNT) {
        return nongTraiDAO.getNongTraiByMaNT(maNT);
    }
    
    public void insertNongTrai(NongTrai nongTrai){
        nongTraiDAO.insertNongTrai(nongTrai);
    }
}
