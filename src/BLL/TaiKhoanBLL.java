package BLL;

import DAO.TaiKhoanDAO;
import DTO.TaiKhoan;
import java.util.List;

public class TaiKhoanBLL {
    private final TaiKhoanDAO taiKhoanDAO;

    public TaiKhoanBLL() {
        taiKhoanDAO = new TaiKhoanDAO();
    }
    
    public List<TaiKhoan> getAllTaiKhoan() {
        return taiKhoanDAO.getAllTaiKhoan();
    }
    
    public TaiKhoan getTaiKhoanByMaTK(String maTK) {
        return taiKhoanDAO.getTaiKhoanByMaTK(maTK);
    }
    
    public void insertTaiKhoan(TaiKhoan taiKhoan){
        taiKhoanDAO.insertTaiKhoan(taiKhoan);
    }
    
    public void deleteTaiKhoan(String maTK){
        taiKhoanDAO.deleteTaiKhoan(maTK);
    }
    
    public void updateTaikhoan(TaiKhoan taikhoan){
        taiKhoanDAO.updateTaikhoan(taikhoan);
    }
}
