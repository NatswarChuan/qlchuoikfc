package BLL;

import DAO.SanPhamDAO;
import DTO.SanPham;
import java.util.List;

public class SanPhamBLL {
    private SanPhamDAO monAnDAO;

    public SanPhamBLL() {
        monAnDAO = new SanPhamDAO();
    }
    
    public List<SanPham> getAllMonAn() {
        return monAnDAO.getAllMonAn();
    }
    
    public SanPham getMonAnByMaMA(String maMA) {
        return monAnDAO.getMonAnByMaMA(maMA);
    }
    
    public void insertMonAn(SanPham monAn){
        monAnDAO.insertMonAn(monAn);
    } 
    
    public void deleteMonAn(String maMA){
        monAnDAO.deleteMonAn(maMA);
    }
    
    public void updateMonAn(SanPham monAn){
        monAnDAO.updateMonAn(monAn);
    }
}
