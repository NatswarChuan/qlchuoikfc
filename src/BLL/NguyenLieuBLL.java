package BLL;

import DAO.NguyenLieuDAO;
import DTO.NguyenLieu;
import java.util.List;

public class NguyenLieuBLL {
    private NguyenLieuDAO nguyenLieuDAO;

    public NguyenLieuBLL() {
        nguyenLieuDAO = new NguyenLieuDAO();
    }
    
    public List<NguyenLieu> getAllNguyenLieu() {
        return nguyenLieuDAO.getAllNguyenLieu();
    }
    
    public NguyenLieu getNguyenLieuByMaNL(String maNL) {
        return nguyenLieuDAO.getNguyenLieuByMaNL(maNL);
    }
    
    public void insertNguyenLieu(NguyenLieu nguyenLieu){
        nguyenLieuDAO.insertNguyenLieu(nguyenLieu);
    }
    
    public void deleteNguyenLieu(String maNL){
        nguyenLieuDAO.deleteNguyenLieu(maNL);
    }
    
    public void updateNguyenLieu(NguyenLieu nguyenLieu){
        nguyenLieuDAO.updateNguyenLieu(nguyenLieu);
    } 
}
