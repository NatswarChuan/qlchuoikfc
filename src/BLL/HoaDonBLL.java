package BLL;

import DAO.HoaDonDAO;
import DTO.HoaDon;
import java.util.List;

public class HoaDonBLL {
    private HoaDonDAO hoaDonDAO;

    public HoaDonBLL() {
        hoaDonDAO = new HoaDonDAO();
    }
    
    public List<HoaDon> getAllHoaDon() {
        return hoaDonDAO.getAllHoaDon();
    }
    
    public HoaDon getHoaDonByMaHD(String maNL) {
        return hoaDonDAO.getHoaDonByMaHD(maNL);
    }
    
    public void insertHoaDon(HoaDon hoaDon){
        hoaDonDAO.insertHoaDon(hoaDon);
    }
}
