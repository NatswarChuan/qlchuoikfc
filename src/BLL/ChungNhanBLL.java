package BLL;

import DAO.ChungNhanDAO;
import DTO.ChungNhan;
import java.util.List;

public class ChungNhanBLL {
    private ChungNhanDAO chungNhanDAO;

    public ChungNhanBLL() {
        chungNhanDAO = new ChungNhanDAO();
    }
    
    public List<ChungNhan> getAllChungNhan() {
        return chungNhanDAO.getAllChungNhan();
    }
    
    public ChungNhan getChungNhanByMaCN(String maCN) {
        return chungNhanDAO.getChungNhanByMaCN(maCN);
    }
    
    
}
