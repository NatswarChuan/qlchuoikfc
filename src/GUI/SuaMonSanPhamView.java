package GUI;

import BLL.ChungNhanBLL;
import BLL.SanPhamBLL;
import BLL.NguonGocBLL;
import BLL.NguyenLieuBLL;
import DTO.ChungNhan;
import DTO.SanPham;
import DTO.NguonGoc;
import DTO.NguyenLieu;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class SuaMonSanPhamView extends javax.swing.JFrame {

    SanPhamBLL monAnBLL;
    SanPham monAn;
    ChungNhan chungNhan;
    NguyenLieu nguyenLieu;
    List<NguyenLieu> nguyenLieus;
    NguonGoc nguonGoc;
    List<NguonGoc> nguonGocs;
    LinkedHashMap<Integer, String> maNL_map;
    ChungNhanBLL chungNhanBLL;
    NguyenLieuBLL nguyenLieuBLL;
    NguonGocBLL nguonGocBLL;
    DefaultTableModel defaultTableModel;

    String maTaiKhoan;

    public SuaMonSanPhamView(String maMonAn, String maTK) throws ParseException {
        initComponents();
Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;

        tbNguyenLieu.setComponentPopupMenu(popupMenuNguyenLieu);

        monAnBLL = new SanPhamBLL();
        nguonGocs = new ArrayList<>();
        nguyenLieuBLL = new NguyenLieuBLL();
        chungNhanBLL = new ChungNhanBLL();
        nguonGocBLL = new NguonGocBLL();
        maNL_map = new LinkedHashMap<>();

        monAn = monAnBLL.getMonAnByMaMA(maMonAn);
        nguonGocs = nguonGocBLL.getNguonGocbyMaMA(maMonAn);

        for (int i = 0; i < nguonGocs.size(); i++) {
            maNL_map.put(i, nguonGocs.get(i).getMaNL());
        }

        Date ngayCB_format = monAn.formatStringDate(monAn.getTgCheBien());

        txtMaMonAn.setText(monAn.getMaMonAn());
        txtTenMonAn.setText(monAn.getTenMonAn());
        txtMaNH.setText(monAn.getMaNH());
        chooseDateNgayCB.setDate(ngayCB_format);

        if (monAn.getKiemNghiem().equals("Có")) {
            rdCo.setSelected(true);
        }
        if (monAn.getKiemNghiem().equals("Không")) {
            rdKhong.setSelected(true);
        }

        List<NguyenLieu> nguyenLieus = nguyenLieuBLL.getAllNguyenLieu();
        for (NguyenLieu nguyenLieu1 : nguyenLieus) {
            cbNguyenLieu.addItem(nguyenLieu1.getMaNL());
        }

        List<ChungNhan> chungNhans = chungNhanBLL.getAllChungNhan();
        for (ChungNhan chungNhan : chungNhans) {
            cbMaCN.addItem(chungNhan.getMaCN());
        }

        defaultTableModel = new DefaultTableModel() {
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tbNguyenLieu.setModel(defaultTableModel);

        defaultTableModel.addColumn("MÃ NGUYÊN LIỆU");
        defaultTableModel.addColumn("TÊN NGUYÊN LIỆU");
        defaultTableModel.addColumn("MÃ NÔNG TRẠI");
        defaultTableModel.addColumn("MÃ TIÊU CHUẨN");
        defaultTableModel.addColumn("MÃ CHỨNG NHẬN");
        defaultTableModel.addColumn("HỆ THỐNG GIÁM SÁT");
        defaultTableModel.addColumn("HÌNH THỨC CANH TÁC");
        defaultTableModel.addColumn("THỜI GIAN THU HOẠCH");
        defaultTableModel.addColumn("THỜI GIAN SƠ CHẾ");
        defaultTableModel.addColumn("THỜI GIAN ĐÓNG GÓI");

        nguyenLieu = new NguyenLieu();
        for (NguonGoc nguonGoc1 : nguonGocs) {
            nguyenLieu = nguyenLieuBLL.getNguyenLieuByMaNL(nguonGoc1.getMaNL());
            loadTableData();
        }
    }

    private void loadTableData() {
        DefaultTableModel model = (DefaultTableModel) tbNguyenLieu.getModel();

        model.addRow(new Object[]{nguyenLieu.getMaNL(), nguyenLieu.getTenNL(), nguyenLieu.getMaNT(), nguyenLieu.getMaTC(), nguyenLieu.getMaCN()
        });

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupMenuNguyenLieu = new javax.swing.JPopupMenu();
        menuItemXoa = new javax.swing.JMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtMaMonAn = new javax.swing.JTextField();
        txtTenCN = new javax.swing.JTextField();
        chooseDateNgayCB = new com.toedter.calendar.JDateChooser();
        lbNoti1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        cbMaCN = new javax.swing.JComboBox<>();
        txtMaNH = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        rdCo = new javax.swing.JRadioButton();
        rdKhong = new javax.swing.JRadioButton();
        lbNoti2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtTenMonAn = new javax.swing.JTextField();
        lbNoti4 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        cbNguyenLieu = new javax.swing.JComboBox<>();
        btnChon = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtTenNL = new javax.swing.JTextField();
        lbNoti3 = new javax.swing.JLabel();
        jPanel12 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbNguyenLieu = new javax.swing.JTable();
        btnSua = new javax.swing.JButton();
        btnTroLai = new javax.swing.JButton();

        menuItemXoa.setText("Xóa");
        menuItemXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuItemXoaActionPerformed(evt);
            }
        });
        popupMenuNguyenLieu.add(menuItemXoa);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin chung", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel29.setText("Mã món ăn:");

        jLabel30.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel30.setText("Mã nhà hàng:");

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel31.setText("Ngày chế biến:");

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel32.setText("Tên chứng nhận:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Mã chứng nhận:");

        txtMaMonAn.setEditable(false);
        txtMaMonAn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMaMonAnActionPerformed(evt);
            }
        });

        txtTenCN.setEditable(false);
        txtTenCN.setText("    ");

        lbNoti1.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti1.setText("   ");

        jLabel5.setForeground(new java.awt.Color(255, 0, 0));
        jLabel5.setText("   ");

        cbMaCN.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Chọn mã chứng nhận" }));
        cbMaCN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMaCNActionPerformed(evt);
            }
        });

        txtMaNH.setEditable(false);
        txtMaNH.setText("  ");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Kiểm chứng:");

        rdCo.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rdCo);
        rdCo.setSelected(true);
        rdCo.setText("Có");

        rdKhong.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rdKhong);
        rdKhong.setText("Không");
        rdKhong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdKhongActionPerformed(evt);
            }
        });

        lbNoti2.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti2.setText("   ");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Tên món ăn:");

        lbNoti4.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti4.setText("   ");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap(63, Short.MAX_VALUE)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel31)
                    .addComponent(jLabel29)
                    .addComponent(jLabel32)
                    .addComponent(jLabel3)
                    .addComponent(jLabel30)
                    .addComponent(jLabel6)
                    .addComponent(jLabel4))
                .addGap(58, 58, 58)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbNoti2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbMaCN, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(lbNoti4, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtTenMonAn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbNoti1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txtMaMonAn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel15Layout.createSequentialGroup()
                            .addComponent(rdCo)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 110, Short.MAX_VALUE)
                            .addComponent(rdKhong))
                        .addComponent(txtMaNH, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txtTenCN, javax.swing.GroupLayout.Alignment.LEADING))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(chooseDateNgayCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(36, 36, 36))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(txtMaMonAn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti1)
                .addGap(8, 8, 8)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(txtTenMonAn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(chooseDateNgayCB, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel31)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbMaCN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti2)
                .addGap(14, 14, 14)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTenCN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32))
                .addGap(18, 18, 18)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(rdKhong)
                    .addComponent(rdCo)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMaNH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30))
                .addContainerGap())
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin món ăn", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel21.setText("Mã nguyên liệu:");

        cbNguyenLieu.setMaximumRowCount(50);
        cbNguyenLieu.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Chọn nguyên liệu" }));
        cbNguyenLieu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbNguyenLieuActionPerformed(evt);
            }
        });

        btnChon.setBackground(new java.awt.Color(204, 255, 153));
        btnChon.setText("Chọn");
        btnChon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChonActionPerformed(evt);
            }
        });

        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("   ");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Tên nguyên liệu:");

        txtTenNL.setEditable(false);
        txtTenNL.setText("    ");

        lbNoti3.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti3.setText("   ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel21)
                    .addComponent(jLabel2))
                .addGap(31, 31, 31)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbNoti3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cbNguyenLieu, 0, 180, Short.MAX_VALUE)
                    .addComponent(btnChon)
                    .addComponent(txtTenNL))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(67, 67, 67))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cbNguyenLieu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbNoti3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtTenNL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(63, 63, 63)
                .addComponent(btnChon)
                .addGap(56, 56, 56))
        );

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel26.setText("SỬA MÓN ĂN");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(389, Short.MAX_VALUE)
                .addComponent(jLabel26)
                .addGap(347, 347, 347))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel26)
                .addContainerGap())
        );

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Danh sách nguyên liệu", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbNguyenLieu.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "", "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbNguyenLieu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbNguyenLieuMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbNguyenLieu);

        btnSua.setText("Sửa");
        btnSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaActionPerformed(evt);
            }
        });

        btnTroLai.setBackground(new java.awt.Color(204, 255, 153));
        btnTroLai.setText("Trở lại");
        btnTroLai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroLaiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnTroLai)
                .addContainerGap())
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(428, 428, 428)
                .addComponent(btnSua)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jScrollPane4)
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSua)
                .addGap(5, 5, 5)
                .addComponent(btnTroLai)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtMaMonAnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMaMonAnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaMonAnActionPerformed

    private void cbMaCNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbMaCNActionPerformed
        // TODO add your handling code here:
        if (cbMaCN.getSelectedIndex() == 0) {
            lbNoti2.setText("Chọn mã chứng nhận!");
            txtTenCN.setText(" ");
        } else {
            ChungNhan cn = chungNhanBLL.getChungNhanByMaCN(String.valueOf(cbMaCN.getSelectedItem()));
            txtTenCN.setText(cn.getTenCN());
            lbNoti2.setText(" ");
        }
    }//GEN-LAST:event_cbMaCNActionPerformed

    private void rdKhongActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdKhongActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rdKhongActionPerformed

    private void cbNguyenLieuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbNguyenLieuActionPerformed
        // TODO add your handling code here:
        if (cbNguyenLieu.getSelectedIndex() == 0) {
            lbNoti3.setText("Vui lòng chọn mã nguyên liệu!");
            txtTenNL.setText(" ");
        } else {
            nguyenLieu = nguyenLieuBLL.getNguyenLieuByMaNL(String.valueOf(cbNguyenLieu.getSelectedItem()));
            txtTenNL.setText(nguyenLieu.getTenNL());
            lbNoti3.setText(" ");
        }
    }//GEN-LAST:event_cbNguyenLieuActionPerformed

    private void btnChonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChonActionPerformed
        // TODO add your handling code here:
        // chưa xử lý trùng nguyên liệu
        boolean check = true;

        if (cbNguyenLieu.getSelectedIndex() != 0) {
            nguonGoc = new NguonGoc();
            nguonGoc.setMaMonAn(txtMaMonAn.getText());
            nguonGoc.setMaNL(String.valueOf(cbNguyenLieu.getSelectedItem()));

            for (int i = 0; i < nguonGocs.size(); i++) {
                if (nguonGocs.get(i).getMaNL().equals(nguonGoc.getMaNL())) {
                    lbNoti3.setText("Nguyên liệu đã tồn tại!");
                    check = false;
                    break;
                }
            }

            if (check == true) {
                loadTableData();
                nguonGocs.add(nguonGoc);
                nguonGocBLL.insertNguonGoc(nguonGoc);
            }
        } else {
            lbNoti3.setText("Vui lòng chọn mã nguyên liệu!");
            txtTenNL.setText(" ");
        }
    }//GEN-LAST:event_btnChonActionPerformed

    private void tbNguyenLieuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbNguyenLieuMouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_tbNguyenLieuMouseClicked

    private void btnSuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaActionPerformed
        String maMonAn = txtMaMonAn.getText().trim();
        String tenMonAn = txtTenMonAn.getText().trim();

        Date ngayCB = chooseDateNgayCB.getDate();

        if (maMonAn.equals("")) {
            lbNoti1.setText("Vui lòng nhập mã món ăn!");
        }
        if (tenMonAn.equals("")) {
            lbNoti4.setText("Vui lòng nhập tên món ăn!");
        }

        if (cbMaCN.getSelectedIndex() == 0) {
            lbNoti2.setText("Chọn mã chứng nhận!");
            txtTenCN.setText(" ");
        }

        if ((!maMonAn.equals("")) && (cbMaCN.getSelectedIndex() != 0)) {
            monAn.setMaCN(String.valueOf(cbMaCN.getSelectedItem()));
            monAn.setMaMonAn(maMonAn);
            monAn.setTenMonAn(tenMonAn);

            if (rdCo.isSelected()) {
                monAn.setKiemNghiem("Có");
            }
            if (rdKhong.isSelected()) {
                monAn.setKiemNghiem("Không");
            }

            try {
                String ngayCBFormat = monAn.formatDateString(ngayCB);
                monAn.setTgCheBien(ngayCBFormat);

            } catch (ParseException ex) {
            }

            // Phải đúng mã nhà hàng, do là khóa ngoại
            monAn.setMaNH(maTaiKhoan);

            monAnBLL.updateMonAn(monAn);

            JOptionPane.showMessageDialog(SuaMonSanPhamView.this, "Sửa nguyên liệu thành công!", "Notification", JOptionPane.INFORMATION_MESSAGE);

            lbNoti1.setText(" ");
            lbNoti2.setText(" ");
            lbNoti3.setText(" ");
            lbNoti4.setText(" ");
            nguonGocs = new ArrayList<>();
            txtMaMonAn.setText("");
            txtTenMonAn.setText("");
            defaultTableModel.setRowCount(0);

        } else {
            JOptionPane.showMessageDialog(SuaMonSanPhamView.this, "Sửa nguyên liệu thất bại!", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_btnSuaActionPerformed

    private void btnTroLaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroLaiActionPerformed
        // TODO add your handling code here:
        new QLNhaHangView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnTroLaiActionPerformed

    private void menuItemXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuItemXoaActionPerformed
        // TODO add your handling code here:
        int i = tbNguyenLieu.getSelectedRow();

        defaultTableModel.removeRow(i);
        nguonGocBLL.deleteNguonGoc(nguonGocs.get(i).getMaMonAn(), nguonGocs.get(i).getMaNL());
        nguonGocs.remove(nguonGocs.get(i));
    }//GEN-LAST:event_menuItemXoaActionPerformed

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChon;
    private javax.swing.JButton btnSua;
    private javax.swing.JButton btnTroLai;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cbMaCN;
    private javax.swing.JComboBox<String> cbNguyenLieu;
    private com.toedter.calendar.JDateChooser chooseDateNgayCB;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lbNoti1;
    private javax.swing.JLabel lbNoti2;
    private javax.swing.JLabel lbNoti3;
    private javax.swing.JLabel lbNoti4;
    private javax.swing.JMenuItem menuItemXoa;
    private javax.swing.JPopupMenu popupMenuNguyenLieu;
    private javax.swing.JRadioButton rdCo;
    private javax.swing.JRadioButton rdKhong;
    private javax.swing.JTable tbNguyenLieu;
    private javax.swing.JTextField txtMaMonAn;
    private javax.swing.JTextField txtMaNH;
    private javax.swing.JTextField txtTenCN;
    private javax.swing.JTextField txtTenMonAn;
    private javax.swing.JTextField txtTenNL;
    // End of variables declaration//GEN-END:variables
}
