package GUI;

import BLL.TaiKhoanBLL;
import DTO.TaiKhoan;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JOptionPane;

public class DoiMatKhauView extends javax.swing.JFrame {

    TaiKhoanBLL taiKhoanBLL;
    TaiKhoan taiKhoan;
    
    String maTaiKhoan;
    
    public DoiMatKhauView(String maTK) {
        initComponents();
        Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;
        
        taiKhoanBLL = new TaiKhoanBLL();
        taiKhoan = new TaiKhoan();
        taiKhoan = taiKhoanBLL.getTaiKhoanByMaTK(maTK);
        
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnOK = new javax.swing.JButton();
        pfOldPass = new javax.swing.JPasswordField();
        pfNewPass = new javax.swing.JPasswordField();
        pfNewPassAgain = new javax.swing.JPasswordField();
        lbNoti1 = new javax.swing.JLabel();
        lbNoti2 = new javax.swing.JLabel();
        lbNoti3 = new javax.swing.JLabel();
        btnTroLai = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel1.setText("ĐỔI MẬT KHẨU");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Mật khẩu cũ:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Mật khẩu mới:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Nhập lại mật khẩu mới:");

        btnOK.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btnOK.setText("OK");
        btnOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOKActionPerformed(evt);
            }
        });

        pfOldPass.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pfOldPass.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pfOldPassActionPerformed(evt);
            }
        });

        pfNewPass.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        pfNewPassAgain.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        lbNoti1.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti1.setText("    ");

        lbNoti2.setText("   ");

        lbNoti3.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti3.setText("   ");

        btnTroLai.setText("TRỞ LẠI");
        btnTroLai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroLaiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbNoti3, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                    .addComponent(lbNoti2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbNoti1, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                    .addComponent(btnOK)
                    .addComponent(pfOldPass)
                    .addComponent(pfNewPass)
                    .addComponent(pfNewPassAgain))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(149, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(124, 124, 124))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnTroLai)
                        .addGap(25, 25, 25))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(pfOldPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti1)
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(pfNewPass, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti2)
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(pfNewPassAgain, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti3)
                .addGap(24, 24, 24)
                .addComponent(btnOK)
                .addGap(18, 18, 18)
                .addComponent(btnTroLai)
                .addContainerGap(14, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void pfOldPassActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pfOldPassActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pfOldPassActionPerformed

    private void btnOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOKActionPerformed
        // TODO add your handling code here:
        String oldPass = pfOldPass.getText();
        String newPass = pfNewPass.getText();
        String newPassAgain = pfNewPassAgain.getText();
        
        System.out.println("old pass: " + oldPass);
        System.out.println("new pass: " + newPass);
        System.out.println("new pass again: " + newPassAgain);
        
        if (oldPass.equals(taiKhoan.getPassword())) {
            if (newPassAgain.equals(newPass)) {
                taiKhoan.setPassword(newPass);
                taiKhoanBLL.updateTaikhoan(taiKhoan);
                lbNoti1.setText("   ");
                lbNoti3.setText("   ");
                JOptionPane.showMessageDialog(DoiMatKhauView.this, "Đổi mật khẩu thành công!", "Notification", JOptionPane.INFORMATION_MESSAGE);
            } 
            lbNoti1.setText("   ");
        } else {
            lbNoti1.setText("Mật khẩu không trùng khớp!");
        } 
        
        if (newPassAgain.equals(newPass) == false) {
            lbNoti3.setText("Mật khẩu mới không trùng khớp!");
        } else {
            lbNoti3.setText("   ");
        }
    }//GEN-LAST:event_btnOKActionPerformed

    private void btnTroLaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroLaiActionPerformed
        // TODO add your handling code here:
        if (taiKhoan.getVaitro().equals("ADMIN")) {
                new MainAdmin(maTaiKhoan).setVisible(true);
                this.dispose();
        }
        if (taiKhoan.getVaitro().equals("Nông trại")) {
            new NguyenLieuView(maTaiKhoan).setVisible(true);
            this.dispose();
        } 
        if (taiKhoan.getVaitro().equals("Nhà phân phối")) {
            new NhapHangNPPView(maTaiKhoan).setVisible(true);
            this.dispose();
        }
        if (taiKhoan.getVaitro().equals("Quản lý nhà hàng")) {
            new QLNhaHangView(maTaiKhoan).setVisible(true);
            this.dispose();
        }
        
        this.dispose();
    }//GEN-LAST:event_btnTroLaiActionPerformed

//    public static void main(String args[]) {
//        new DoiMatKhauView("TK001").setVisible(true);
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnOK;
    private javax.swing.JButton btnTroLai;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel lbNoti1;
    private javax.swing.JLabel lbNoti2;
    private javax.swing.JLabel lbNoti3;
    private javax.swing.JPasswordField pfNewPass;
    private javax.swing.JPasswordField pfNewPassAgain;
    private javax.swing.JPasswordField pfOldPass;
    // End of variables declaration//GEN-END:variables
}
