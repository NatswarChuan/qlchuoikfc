package GUI;

import BLL.NguyenLieuBLL;
import DTO.NguyenLieu;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.ParseException;
import java.util.Date;
import javax.swing.JOptionPane;

public class SuaNguyenLieuView extends javax.swing.JFrame {

    NguyenLieu nguyenLieu;
    NguyenLieuBLL nguyenLieuBLL;

    String maTaiKhoan;

    public SuaNguyenLieuView(String maNL, String maTK) throws ParseException {
        initComponents();
Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;

        nguyenLieuBLL = new NguyenLieuBLL();
        nguyenLieu = nguyenLieuBLL.getNguyenLieuByMaNL(maNL);

        txtMaNL.setText(nguyenLieu.getMaNL());
        txtTenNL.setText(nguyenLieu.getTenNL());
        txtMaNT.setText(nguyenLieu.getMaNT());
        txtMaTC.setText(nguyenLieu.getMaTC());
        txtMaCN.setText(nguyenLieu.getMaCN());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtMaNL = new javax.swing.JTextField();
        txtTenNL = new javax.swing.JTextField();
        txtMaNT = new javax.swing.JTextField();
        txtMaTC = new javax.swing.JTextField();
        txtMaCN = new javax.swing.JTextField();
        btnSua = new javax.swing.JButton();
        lbNoti1 = new javax.swing.JLabel();
        lbNoti2 = new javax.swing.JLabel();
        lbNoti3 = new javax.swing.JLabel();
        lbNoti4 = new javax.swing.JLabel();
        lbNoti5 = new javax.swing.JLabel();
        lbNoti6 = new javax.swing.JLabel();
        lbNoti7 = new javax.swing.JLabel();
        lbNoti8 = new javax.swing.JLabel();
        lbNoti9 = new javax.swing.JLabel();
        lbNoti10 = new javax.swing.JLabel();
        btnTroLai = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel1.setText("SỬA NGUYÊN LIỆU");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(121, 121, 121))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Mã nguyên liệu:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Tên nguyên liệu:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Mã nông trại:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Mã tiêu chuẩn:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Mã chứng nhận:");

        txtMaNL.setEditable(false);

        btnSua.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnSua.setText("SỬA");
        btnSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaActionPerformed(evt);
            }
        });

        lbNoti1.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti1.setText("   ");

        lbNoti2.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti2.setText("  ");

        lbNoti3.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti3.setText("  ");

        lbNoti4.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti4.setText("   ");

        lbNoti5.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti5.setText("   ");

        lbNoti6.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti6.setText("   ");

        lbNoti7.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti7.setText("   ");

        lbNoti8.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti8.setText("   ");

        lbNoti9.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti9.setText("   ");

        lbNoti10.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti10.setText("   ");

        btnTroLai.setText("TRỞ LẠI");
        btnTroLai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroLaiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addGap(71, 71, 71)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btnSua)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbNoti10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbNoti9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbNoti8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(lbNoti7, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                                .addGap(120, 120, 120))
                            .addComponent(lbNoti6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbNoti5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbNoti4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbNoti3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbNoti2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbNoti1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtMaTC, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtMaCN, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTenNL, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtMaNT, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtMaNL, javax.swing.GroupLayout.Alignment.LEADING))
                        .addGap(82, 82, 82))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnTroLai)
                .addGap(23, 23, 23))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtMaNL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(lbNoti1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTenNL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMaNT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMaTC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMaCN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti5)
                .addGap(33, 33, 33)
                .addComponent(lbNoti6)
                .addGap(33, 33, 33)
                .addComponent(lbNoti7)
                .addGap(40, 40, 40)
                .addComponent(lbNoti8)
                .addGap(34, 34, 34)
                .addComponent(lbNoti9)
                .addGap(34, 34, 34)
                .addComponent(lbNoti10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSua)
                .addGap(18, 18, 18)
                .addComponent(btnTroLai)
                .addContainerGap(20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaActionPerformed
        // TODO add your handling code here:
        String maNL = txtMaNL.getText().trim();
        String tenNL = txtTenNL.getText().trim();
        String maNT = txtMaNT.getText().trim();
        String maTC = txtMaTC.getText().trim();
        String maCN = txtMaCN.getText().trim();
        //System.out.println(tgth + "    " + tgsc + "   " + tgdg);

        if (maNL.equals("")) {
            lbNoti1.setText("Vui lòng nhập mã nguyên liệu!");
        } else {
            lbNoti1.setText(" ");
        }

        if (tenNL.equals("")) {
            lbNoti2.setText("Vui lòng nhập tên nguyên liệu!");
        } else {
            lbNoti2.setText(" ");
        }

        if (maNT.equals("")) {
            lbNoti3.setText("Vui lòng nhập mã nông trại!");
        } else {
            lbNoti3.setText(" ");
        }

        if (maTC.equals("")) {
            lbNoti4.setText("Vui lòng nhập mã tiêu chuẩn!");
        } else {
            lbNoti4.setText(" ");
        }

        if (maCN.equals("")) {
            lbNoti5.setText("Vui lòng nhập mã chứng nhận!");
        } else {
            lbNoti5.setText(" ");
        }

        // chưa bắt lỗi trùng mã nguyên liệu, lỗi ngày sơ chế, đóng gói trước ngày thu hoạch
        if (!maNL.equals("") && !tenNL.equals("") && !maNT.equals("") && !maTC.equals("") && !maCN.equals("")) {
            nguyenLieu.setMaNL(maNL);
            nguyenLieu.setTenNL(tenNL);
            nguyenLieu.setMaNT(maNT);
            nguyenLieu.setMaTC(maTC);
            nguyenLieu.setMaCN(maCN);

            nguyenLieuBLL.updateNguyenLieu(nguyenLieu);
            JOptionPane.showMessageDialog(SuaNguyenLieuView.this, "Sửa nguyên liệu thành công!", "Notification", JOptionPane.INFORMATION_MESSAGE);
            lbNoti10.setText(" ");
        }
    }//GEN-LAST:event_btnSuaActionPerformed

    private void btnTroLaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroLaiActionPerformed
        // TODO add your handling code here:
        new NguyenLieuView(maTaiKhoan).setVisible(true);
    }//GEN-LAST:event_btnTroLaiActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSua;
    private javax.swing.JButton btnTroLai;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbNoti1;
    private javax.swing.JLabel lbNoti10;
    private javax.swing.JLabel lbNoti2;
    private javax.swing.JLabel lbNoti3;
    private javax.swing.JLabel lbNoti4;
    private javax.swing.JLabel lbNoti5;
    private javax.swing.JLabel lbNoti6;
    private javax.swing.JLabel lbNoti7;
    private javax.swing.JLabel lbNoti8;
    private javax.swing.JLabel lbNoti9;
    private javax.swing.JTextField txtMaCN;
    private javax.swing.JTextField txtMaNL;
    private javax.swing.JTextField txtMaNT;
    private javax.swing.JTextField txtMaTC;
    private javax.swing.JTextField txtTenNL;
    // End of variables declaration//GEN-END:variables
}
