package GUI;

import BLL.NongTraiBLL;
import BLL.TaiKhoanBLL;
import DTO.NongTrai;
import DTO.TaiKhoan;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class ThemNongTraiView extends javax.swing.JFrame {

    NongTrai nongTrai;
    NongTraiBLL nongTraiBLL;
    List<NongTrai> nongTrais;
    List<TaiKhoan> taiKhoans;
    TaiKhoanBLL taiKhoanBLL;
    
    String maTaiKhoan;
    
    public ThemNongTraiView(String maTK) {
        initComponents();
        Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;
        
        nongTrai = new NongTrai();
        nongTraiBLL = new NongTraiBLL();
        nongTrais = new ArrayList<>();
        nongTrais = nongTraiBLL.getAllNongTrai();
        
        taiKhoanBLL = new TaiKhoanBLL();
        taiKhoans = taiKhoanBLL.getAllTaiKhoan();
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnTroLai = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtMaNT = new javax.swing.JTextField();
        txtTenNT = new javax.swing.JTextField();
        txtChuSH = new javax.swing.JTextField();
        txtDiachi = new javax.swing.JTextField();
        txtSDT = new javax.swing.JTextField();
        txtMaTK = new javax.swing.JTextField();
        lbNoti1 = new javax.swing.JLabel();
        lbNoti6 = new javax.swing.JLabel();
        lbNoti5 = new javax.swing.JLabel();
        lbNoti4 = new javax.swing.JLabel();
        lbNoti3 = new javax.swing.JLabel();
        lbNoti2 = new javax.swing.JLabel();
        lbNoti7 = new javax.swing.JLabel();
        btnThem = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnTroLai.setText("TRỞ LẠI");
        btnTroLai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroLaiActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel1.setText("THÊM NÔNG TRẠI");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(117, 117, 117)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Mã nông trại:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Tên nông trại:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Chủ sỡ hữu:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Địa chỉ:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Số điện thoại:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Mã tài khoản:");

        lbNoti1.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti1.setText("   ");

        lbNoti6.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti6.setText("   ");

        lbNoti5.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti5.setText("   ");

        lbNoti4.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti4.setText("   ");

        lbNoti3.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti3.setText("   ");

        lbNoti2.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti2.setText("   ");

        lbNoti7.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti7.setText("  ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addGap(56, 56, 56)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbNoti7, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lbNoti3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbNoti5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbNoti6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtMaNT)
                        .addComponent(txtChuSH)
                        .addComponent(txtDiachi, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                        .addComponent(txtSDT, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE)
                        .addComponent(txtMaTK, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(lbNoti1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtTenNT, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(lbNoti2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbNoti4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(88, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtMaNT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addComponent(lbNoti1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTenNT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti2)
                .addGap(5, 5, 5)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtChuSH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(lbNoti3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtDiachi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti4)
                .addGap(5, 5, 5)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtSDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMaTK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti7)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        btnThem.setText("THÊM");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnTroLai)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btnThem)
                        .addGap(191, 191, 191))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnThem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 31, Short.MAX_VALUE)
                .addComponent(btnTroLai)
                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        // TODO add your handling code here:
        String maNT = txtMaNT.getText().trim();
        String tenNT = txtTenNT.getText().trim();
        String chuSoHuu = txtChuSH.getText().trim();
        String diaChi = txtDiachi.getText().trim();
        String sdt = txtSDT.getText().trim();
        String maTK = txtMaTK.getText().trim();
        boolean check = true;
        
        if (maNT.equals("")) {
            lbNoti1.setText("Vui lòng nhập mã nông trại!");
        } else {
            lbNoti1.setText(" ");
        }
        
        if (tenNT.equals("")) {
            lbNoti2.setText("Vui lòng nhập tên nông trại!");
        } else {
            lbNoti2.setText(" ");
        }
        
        if (chuSoHuu.equals("")) {
            lbNoti3.setText("Vui lòng nhập chủ sỡ hữu!");
        } else {
            lbNoti3.setText(" ");
        }
        
        if (diaChi.equals("")) {
            lbNoti4.setText("Vui lòng nhập địa chỉ!");
        } else {
            lbNoti4.setText(" ");
        }
        
        if (sdt.equals("")) {
            lbNoti5.setText("Vui lòng nhập số điện thoại!");
        } else {
            lbNoti5.setText(" ");
        }
        
        if (maTK.equals("")) {
            lbNoti6.setText("Vui lòng nhập mã tài khoản!");
        } else {
            lbNoti6.setText(" ");
        }
        // chưa bắt lỗi trùng mã nông trại
        if (!maNT.equals("") && !tenNT.equals("") && !chuSoHuu.equals("") && !diaChi.equals("") && !sdt.equals("") && !maTK.equals("")) {
            for (int i = 0; i < nongTrais.size(); i++) {
                if (nongTrais.get(i).getMaNT().equals(txtMaNT.getText())) {
                    lbNoti1.setText("Mã nông trại đã tồn tại!");
                    check = false;
                    break;
                } else {
                    lbNoti1.setText("  ");
                }
            }
            
            for (int i = 0; i < taiKhoans.size(); i++) {
                if (!taiKhoans.get(i).getMaTK().equals(txtMaTK.getText())) {                    
                    lbNoti6.setText("Mã tài khoản không tồn tại!");
                    lbNoti7.setText("Vui lòng tạo tài khoản và thử lại.");
                    check = false;
                } else {
                    check = true;
                    lbNoti6.setText(" ");
                    lbNoti7.setText(" ");
                    break;
                }
            }
            
            if (check == true) {
                nongTrai.setMaNT(maNT);
                nongTrai.setTenNT(tenNT);
                nongTrai.setChuSoHuu(chuSoHuu);
                nongTrai.setDiaChi(diaChi);
                nongTrai.setSDT(sdt);
                nongTrai.setMaTK(maTK);

                nongTrais.add(nongTrai);
                nongTraiBLL.insertNongTrai(nongTrai);
                JOptionPane.showMessageDialog(ThemNongTraiView.this, "Thêm nông trại thành công!", "Notification", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(ThemNongTraiView.this, "Thêm nông trại thất bại!", "Error", JOptionPane.ERROR_MESSAGE);           
            } 
        }
    }//GEN-LAST:event_btnThemActionPerformed

    private void btnTroLaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroLaiActionPerformed
        // TODO add your handling code here:
        new NongTraiView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnTroLaiActionPerformed

    //public static void main(String args[]) {
//        new ThemNongTraiView().setVisible(true);
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnTroLai;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbNoti1;
    private javax.swing.JLabel lbNoti2;
    private javax.swing.JLabel lbNoti3;
    private javax.swing.JLabel lbNoti4;
    private javax.swing.JLabel lbNoti5;
    private javax.swing.JLabel lbNoti6;
    private javax.swing.JLabel lbNoti7;
    private javax.swing.JTextField txtChuSH;
    private javax.swing.JTextField txtDiachi;
    private javax.swing.JTextField txtMaNT;
    private javax.swing.JTextField txtMaTK;
    private javax.swing.JTextField txtSDT;
    private javax.swing.JTextField txtTenNT;
    // End of variables declaration//GEN-END:variables
}
