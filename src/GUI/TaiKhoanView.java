package GUI;

import BLL.TaiKhoanBLL;
import DTO.TaiKhoan;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class TaiKhoanView extends javax.swing.JFrame {

    TaiKhoanBLL taiKhoanBLL;
    List<TaiKhoan> taiKhoans;
    
    DefaultTableModel defaultTableModel;
    
    String maTaiKhoan;
    
    public TaiKhoanView(String maTK) {
        initComponents();
                
        maTaiKhoan = maTK;
        Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        taiKhoanBLL = new TaiKhoanBLL();
        taiKhoans = taiKhoanBLL.getAllTaiKhoan();
        
        defaultTableModel = new DefaultTableModel(){
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
         
        tbTK.setModel(defaultTableModel);
        
        defaultTableModel.addColumn("MÃ TÀI KHOẢN");
        defaultTableModel.addColumn("MẬT KHẨU");
        defaultTableModel.addColumn("VAI TRÒ");
        
        setTableData(taiKhoans);
        
        tfMaTK.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if(evt.getKeyCode() == KeyEvent.VK_ENTER){
                    tfPassword.requestFocus();
                }
            }
        });
        
//        tfPassword.addKeyListener(new java.awt.event.KeyAdapter() {
//            public void keyPressed(java.awt.event.KeyEvent evt) {
//                if(evt.getKeyCode() == KeyEvent.VK_ENTER){
//                    tfRole.requestFocus();
//                }
//            }
//        });
        
        
//        lbMore.setComponentPopupMenu(jPopupMenuMore);
//        show(taikhoan.getRole());
    }

    private void setTableData(List<TaiKhoan> taiKhoans){
        for(TaiKhoan taikhoan : taiKhoans){
            defaultTableModel.addRow(new Object[]{taikhoan.getMaTK(), taikhoan.getPassword(), taikhoan.getVaitro()});            
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbTK = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        tfMaTK = new javax.swing.JTextField();
        tfPassword = new javax.swing.JTextField();
        btnThem = new javax.swing.JButton();
        btnSua = new javax.swing.JButton();
        btnXoa = new javax.swing.JButton();
        lbNoti1 = new javax.swing.JLabel();
        lbNoti2 = new javax.swing.JLabel();
        cbVaiTro = new javax.swing.JComboBox<>();
        lbNoti3 = new javax.swing.JLabel();
        btnTroLai = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel1.setText("QUẢN LÝ TÀI KHOẢN");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("TÊN TÀI KHOẢN");

        tbTK.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbTK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbTKMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbTK);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "THÔNG TIN TÀI KHOẢN", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Mã tài khoản:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Mật khẩu:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Vai trò:");

        btnThem.setText("THÊM");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        btnSua.setText("SỬA");
        btnSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaActionPerformed(evt);
            }
        });

        btnXoa.setText("XÓA");
        btnXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaActionPerformed(evt);
            }
        });

        lbNoti1.setForeground(new java.awt.Color(255, 51, 0));
        lbNoti1.setText("  ");
        lbNoti1.setToolTipText("");

        lbNoti2.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti2.setText(" ");

        cbVaiTro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Chọn vai trò", "Nông trại", "Nhà phân phối", "ADMIN" }));
        cbVaiTro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbVaiTroActionPerformed(evt);
            }
        });

        lbNoti3.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti3.setText("    ");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4)))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(btnThem)))
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSua)
                        .addGap(34, 34, 34)
                        .addComponent(btnXoa)
                        .addGap(43, 43, 43))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(27, 27, 27)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lbNoti1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(tfMaTK, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(lbNoti2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tfPassword)
                            .addComponent(cbVaiTro, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbNoti3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(20, 20, 20))))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(tfMaTK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti1)
                .addGap(7, 7, 7)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cbVaiTro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti3)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnThem)
                    .addComponent(btnSua)
                    .addComponent(btnXoa))
                .addContainerGap())
        );

        btnTroLai.setText("TRỞ LẠI");
        btnTroLai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroLaiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnTroLai)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 394, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnTroLai))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(280, 280, 280)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(369, 369, 369)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTroLaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroLaiActionPerformed
        // TODO add your handling code here:
        new MainAdmin(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnTroLaiActionPerformed

    private void btnSuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaActionPerformed
        // TODO add your handling code here:
        
        int row = tbTK.getSelectedRow();
        if(row == -1){
            JOptionPane.showMessageDialog(TaiKhoanView.this, "Vui lòng chọn tài khoản cần sửa", "Error", JOptionPane.ERROR_MESSAGE);
        }
        else{
            TaiKhoan taikhoan;
            taikhoan = taiKhoanBLL.getTaiKhoanByMaTK((String) tbTK.getValueAt(row, 0));//0 do cột mã tài khoản ở cột 0
            DefaultTableModel model = (DefaultTableModel) tbTK.getModel();
            
            switch (cbVaiTro.getSelectedIndex()) {
                case 1 -> taikhoan.setVaitro("Nông trại");
                case 2 -> taikhoan.setVaitro("Nhà phân phối");
                case 3 -> taikhoan.setVaitro("ADMIN");
                default -> {
                }
            }
                        
            //Update trên table
            model.setValueAt(tfMaTK.getText(), row, 0);
            model.setValueAt(tfPassword.getText(), row, 1);
            model.setValueAt(taikhoan.getVaitro(), row, 2);
            
            //Update trên database
            taikhoan.setMaTK(tfMaTK.getText());
            taikhoan.setPassword(tfPassword.getText());
            
            taiKhoanBLL.updateTaikhoan(taikhoan);
            tbTK.setModel(model);
            
        }
    }//GEN-LAST:event_btnSuaActionPerformed

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        // TODO add your handling code here:
        boolean check = true;
        String maTK = tfMaTK.getText().trim();
        String pass = tfPassword.getText().trim();
         
        if("".equals(maTK)){
            lbNoti1.setText("Vui lòng nhập mã tài khoản!");
        } else {
            lbNoti1.setText("   ");
        }

        if("".equals(pass)){
            lbNoti2.setText("Vui lòng nhập mật khẩu!");
        } else {
            lbNoti2.setText("   ");
        }

        if(cbVaiTro.getSelectedIndex() == 0){
            lbNoti3.setText("Vui lòng chọn vai trò!");
        } else {
            lbNoti3.setText("   ");
        }     
        
        if ((!maTK.equals("")) && (!pass.equals("")) && (cbVaiTro.getSelectedIndex()!= 0)) {
            
            for (int i = 0; i < taiKhoans.size(); i++) {
                if (taiKhoans.get(i).getMaTK().equals(tfMaTK.getText())) {
                    lbNoti1.setText("Trùng mã tài khoản!");
                    check = false;
                    break;
                } else {
                    lbNoti1.setText("  ");
                }
            }
            
            if (check == true) {
                DefaultTableModel model = (DefaultTableModel) tbTK.getModel();
                taiKhoanBLL = new TaiKhoanBLL();
                TaiKhoan taikhoan = new TaiKhoan();
                taikhoan.setMaTK(tfMaTK.getText());
                taikhoan.setPassword(tfPassword.getText());

                switch (cbVaiTro.getSelectedIndex()) {
                    case 1 -> taikhoan.setVaitro("Nông trại");
                    case 2 -> taikhoan.setVaitro("Nhà phân phối");
                    case 3 -> taikhoan.setVaitro("Quản lý nhà hàng");
                    case 4 -> taikhoan.setVaitro("ADMIN");
                    default -> {
                    }
                }
         
                JOptionPane.showMessageDialog(TaiKhoanView.this, "Thêm tài khoản thành công!", "Notification", JOptionPane.INFORMATION_MESSAGE);
                model.addRow(new Object[]{taikhoan.getMaTK(), taikhoan.getPassword(), taikhoan.getVaitro()});
                taiKhoans.add(taikhoan);
                taiKhoanBLL.insertTaiKhoan(taikhoan);
                lbNoti1.setText("  ");
                lbNoti2.setText("  ");
                lbNoti3.setText("  ");
            }            
        } else {
            JOptionPane.showMessageDialog(TaiKhoanView.this, "Thêm tài khoản thất bại!", "Error", JOptionPane.ERROR_MESSAGE);           
        } 
    }//GEN-LAST:event_btnThemActionPerformed

    private void cbVaiTroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbVaiTroActionPerformed
        // TODO add your handling code here:
        if (cbVaiTro.getSelectedIndex()== 0) {
            lbNoti3.setText("Chọn vai trò!");
        } else {
            lbNoti3.setText(" ");
        }
    }//GEN-LAST:event_cbVaiTroActionPerformed

    private void tbTKMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbTKMouseClicked
        // TODO add your handling code here:
        int i = tbTK.getSelectedRow();
        if(i >= 0){
            TaiKhoan taikhoan = new TaiKhoan();
            
            taikhoan = taiKhoanBLL.getTaiKhoanByMaTK((String) tbTK.getValueAt(i, 0));//0 do cột mã tài khoản ở cột 0
            tfMaTK.setText(taikhoan.getMaTK());
            tfPassword.setText(taikhoan.getPassword());
            
            if (taikhoan.getVaitro().equals("Nông trại")) {
                cbVaiTro.setSelectedIndex(1);
            } else if (taikhoan.getVaitro().equals("Nhà phân phối")) {
                cbVaiTro.setSelectedIndex(2);
            } else if (taikhoan.getVaitro().equals("Quản lý nhà hàng")) {
                cbVaiTro.setSelectedIndex(3);
            }            
        }
    }//GEN-LAST:event_tbTKMouseClicked

    private void btnXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaActionPerformed
        // TODO add your handling code here:
        int row = tbTK.getSelectedRow();
        if(row == -1){
            JOptionPane.showMessageDialog(TaiKhoanView.this, "Vui lòng chọn tài khoản trước", "Error", JOptionPane.ERROR_MESSAGE);
        }
        else{
            int confirm = JOptionPane.showConfirmDialog(TaiKhoanView.this,"Bạn có chắc chắn muốn xóa không?");

            if(confirm == JOptionPane.YES_OPTION){
                String maTK = String.valueOf(tbTK.getValueAt(row, 0));

                taiKhoanBLL.deleteTaiKhoan(maTK);
                taiKhoans.remove(row);
                defaultTableModel.setRowCount(0);
                setTableData(taiKhoans);
            }
        }
    }//GEN-LAST:event_btnXoaActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        new TaiKhoanView().setVisible(true);
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSua;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnTroLai;
    private javax.swing.JButton btnXoa;
    private javax.swing.JComboBox<String> cbVaiTro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbNoti1;
    private javax.swing.JLabel lbNoti2;
    private javax.swing.JLabel lbNoti3;
    private javax.swing.JTable tbTK;
    private javax.swing.JTextField tfMaTK;
    private javax.swing.JTextField tfPassword;
    // End of variables declaration//GEN-END:variables
}
