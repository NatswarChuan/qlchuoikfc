package GUI;

import BLL.ChiTietHoaDonBLL;
import BLL.ChiTietNhapDLBLL;
import BLL.HoaDonBLL;
import BLL.SanPhamBLL;
import BLL.NguonGocBLL;
import BLL.NguyenLieuBLL;
import BLL.NhapHangDLBLL;
import DTO.ChiTietHoaDon;
import DTO.ChiTietNhapDaiLy;
import DTO.HoaDon;
import DTO.SanPham;
import DTO.NguonGoc;
import DTO.NguyenLieu;
import DTO.NhapHangDaiLY;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class QLNhaHangView extends javax.swing.JFrame {

    List<NhapHangDaiLY> nhapHangNHs;
    NhapHangDLBLL nhapHangNHBLL;
    ChiTietNhapDaiLy chiTietNhapHangNH;
    ChiTietNhapDLBLL ctnhBLL;
    List<ChiTietNhapDaiLy> ctnnhs;
    NguyenLieu nguyenLieuNH;
    NguyenLieuBLL nguyenLieuBLLNH;
    DefaultTableModel defaultTMNH, defaultTMNH1;

    List<SanPham> monAns;
    SanPhamBLL monAnBLLMA;
    List<NguonGoc> nguonGocs;
    NguonGocBLL nguonGocBLL;
    NguyenLieu nguyenLieuMA;
    NguyenLieuBLL nguyenLieuBLLMA;
    DefaultTableModel defaultTMMA, defaultTMMA1;

    SanPham monAn;
    List<HoaDon> hoaDons;
    ChiTietHoaDon chiTietHoaDon;
    List<ChiTietHoaDon> chiTietHoaDons;
    SanPhamBLL monAnBLLHD;
    HoaDonBLL hoaDonBLL;
    ChiTietHoaDonBLL cthdBLL;
    DefaultTableModel defaultTMHD, defaultTMHD1;

    String maTaiKhoan;

    public QLNhaHangView(String maTK) {
        initComponents();
Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;
        System.out.print(maTK);

        // ------------ QUẢN LÝ NHẬP HÀNG ------------- //
        nhapHangNHs = new ArrayList<>();
        nhapHangNHBLL = new NhapHangDLBLL();
        chiTietNhapHangNH = new ChiTietNhapDaiLy();
        ctnhBLL = new ChiTietNhapDLBLL();
        ctnnhs = new ArrayList<>();
        nguyenLieuNH = new NguyenLieu();
        nguyenLieuBLLNH = new NguyenLieuBLL();

        nhapHangNHs = nhapHangNHBLL.getAllNhapHangNHs();

        defaultTMNH = new DefaultTableModel() {
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tbNHNH.setModel(defaultTMNH);
        defaultTMNH.addColumn("MÃ PHIẾU NHẬP NH");
        defaultTMNH.addColumn("MÃ NHÀ PHÂN PHỐI");
        defaultTMNH.addColumn("THỜI GIAN NHẬP");
        defaultTMNH.addColumn("MÃ NHÀ HÀNG");

        setTableDataNhapHang(nhapHangNHs);

        defaultTMNH1 = new DefaultTableModel() {
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tbNguyenLieuNH.setModel(defaultTMNH1);
        defaultTMNH1.addColumn("MÃ NGUYÊN LIỆU");
        defaultTMNH1.addColumn("TÊN NGUYÊN LIỆU");
        defaultTMNH1.addColumn("MÃ NÔNG TRẠI");
        defaultTMNH1.addColumn("MÃ TIÊU CHUẨN");
        defaultTMNH1.addColumn("MÃ CHỨNG NHẬN");
        defaultTMNH1.addColumn("HTGS");
        defaultTMNH1.addColumn("HTCT");
        defaultTMNH1.addColumn("TG THU HOẠCH");
        defaultTMNH1.addColumn("TG SƠ CHẾ");
        defaultTMNH1.addColumn("TG ĐÓNG GÓI");
        defaultTMNH1.addColumn("KHỐI LƯỢNG");
        defaultTMNH1.addColumn("TG VẬN CHUYỂN");

        // -------- QUẢN LÝ MÓN ĂN -------- //
        monAnBLLMA = new SanPhamBLL();
        monAns = new ArrayList<>();
        monAns = monAnBLLMA.getAllMonAn();

        nguonGocs = new ArrayList<>();
        nguonGocBLL = new NguonGocBLL();
        nguonGocs = nguonGocBLL.getAllNguonGoc();

        nguyenLieuMA = new NguyenLieu();
        nguyenLieuBLLMA = new NguyenLieuBLL();

        defaultTMMA = new DefaultTableModel() {
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tbMonAn.setModel(defaultTMMA);

        defaultTMMA.addColumn("MÃ MÓN ĂN");
        defaultTMMA.addColumn("TÊN MÓN ĂN");
        defaultTMMA.addColumn("THỜI GIAN CHẾ BIẾN");
        defaultTMMA.addColumn("MÃ NHÀ HÀNG");
        defaultTMMA.addColumn("MÃ CHỨNG NHẬN");
        defaultTMMA.addColumn("KIỂM NGHIỆM");

        setTableDataMonAn(monAns);

        defaultTMMA1 = new DefaultTableModel() {
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tbNguyenLieuMA.setModel(defaultTMMA1);

        defaultTMMA1.addColumn("MÃ NGUYÊN LIỆU");
        defaultTMMA1.addColumn("TÊN NGUYÊN LIỆU");
        defaultTMMA1.addColumn("MÃ NÔNG TRẠI");
        defaultTMMA1.addColumn("MÃ TIÊU CHUẨN");
        defaultTMMA1.addColumn("MÃ CHỨNG NHẬN");
        defaultTMMA1.addColumn("HỆ THỐNG GIÁM SÁT");
        defaultTMMA1.addColumn("HÌNH THỨC CANH TÁC");
        defaultTMMA1.addColumn("THỜI GIAN THU HOẠCH");
        defaultTMMA1.addColumn("THỜI GIAN SƠ CHẾ");
        defaultTMMA1.addColumn("THỜI GIAN ĐÓNG GÓI");

        // -------- QUẢN LÝ HÓA ĐƠN --------//
        monAn = new SanPham();
        hoaDons = new ArrayList<>();
        chiTietHoaDons = new ArrayList<>();
        hoaDonBLL = new HoaDonBLL();
        cthdBLL = new ChiTietHoaDonBLL();
        monAnBLLHD = new SanPhamBLL();
        chiTietHoaDon = new ChiTietHoaDon();

        hoaDons = hoaDonBLL.getAllHoaDon();

        defaultTMHD = new DefaultTableModel() {
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tbHoaDon.setModel(defaultTMHD);

        defaultTMHD.addColumn("MÃ HÓA ĐƠN");
        defaultTMHD.addColumn("NGÀY XUẤT");
        defaultTMHD.addColumn("TÊN KHÁCH HÀNG");
        defaultTMHD.addColumn("MÃ NHÀ HÀNG");

        defaultTMHD1 = new DefaultTableModel() {
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        setTableDataHD(hoaDons);

        tbMonAnHD.setModel(defaultTMHD1);

        defaultTMHD1.addColumn("MÃ MÓN ĂN");
        defaultTMHD1.addColumn("TÊN MÓN ĂN");
        defaultTMHD1.addColumn("THỜI GIAN CHẾ BIẾN");
        defaultTMHD1.addColumn("MÃ CHỨNG NHẬN");
        defaultTMHD1.addColumn("KIỂM NGHIỆM");
        defaultTMHD1.addColumn("SỐ LƯỢNG");

    }

    private void loadTableDataMAHD(int sl) {
        DefaultTableModel model = (DefaultTableModel) tbMonAnHD.getModel();

        model.addRow(new Object[]{monAn.getMaMonAn(), monAn.getTenMonAn(), monAn.getTgCheBien(), monAn.getMaCN(), monAn.getKiemNghiem(), sl});
    }

    private void setTableDataHD(List<HoaDon> hoaDons) {
        for (HoaDon hoaDon : hoaDons) {
            defaultTMHD.addRow(new Object[]{hoaDon.getMaHD(), hoaDon.getNgayXuat(), hoaDon.getTenKH(), hoaDon.getMaNH()});
        }
    }

    private void setTableDataMonAn(List<SanPham> monAns) {
        for (SanPham monAn : monAns) {
            defaultTMMA.addRow(new Object[]{monAn.getMaMonAn(), monAn.getTenMonAn(), monAn.getTgCheBien(), monAn.getMaNH(), monAn.getMaCN(), monAn.getKiemNghiem()});
        }
    }

    private void setTableDataNL() {
        DefaultTableModel model = (DefaultTableModel) tbNguyenLieuMA.getModel();

        model.addRow(new Object[]{nguyenLieuMA.getMaNL(), nguyenLieuMA.getTenNL(), nguyenLieuMA.getMaNT(), nguyenLieuMA.getMaTC(), nguyenLieuMA.getMaCN(),});

    }

    private void setTableDataNhapHang(List<NhapHangDaiLY> nhapHangNHs) {
        for (NhapHangDaiLY nhapHangNH : nhapHangNHs) {
            defaultTMNH.addRow(new Object[]{nhapHangNH.getMaPNNH(), nhapHangNH.getMaNPP(), nhapHangNH.getTgNhap(), nhapHangNH.getMaNH()});
        }
    }

    private void loadTableDataNH(int klg, String tgvc) {
        DefaultTableModel model = (DefaultTableModel) tbNguyenLieuNH.getModel();

        model.addRow(new Object[]{nguyenLieuNH.getMaNL(), nguyenLieuNH.getTenNL(), nguyenLieuNH.getMaNT(), nguyenLieuNH.getMaTC(), nguyenLieuNH.getMaCN(), klg, tgvc
        });

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbNHNH = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbNguyenLieuNH = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        btnThemNH = new javax.swing.JButton();
        btnThoatNH = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbNguyenLieuMA = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbMonAn = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        btnThemMA = new javax.swing.JButton();
        btnSuaMA = new javax.swing.JButton();
        btnXoaMA = new javax.swing.JButton();
        btnThoatMA = new javax.swing.JButton();
        jPanel9 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbMonAnHD = new javax.swing.JTable();
        jPanel12 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbHoaDon = new javax.swing.JTable();
        jPanel13 = new javax.swing.JPanel();
        btnThemHD = new javax.swing.JButton();
        btnThoatHD = new javax.swing.JButton();
        jMenuBar2 = new javax.swing.JMenuBar();
        menuDX = new javax.swing.JMenu();
        menuDoiMK = new javax.swing.JMenu();

        jMenu1.setText("File");
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jTabbedPane1.setToolTipText("");
        jTabbedPane1.setName(""); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel1.setText("QUẢN LÝ NHẬP HÀNG NHÀ HÀNG");

        tbNHNH.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbNHNH.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbNHNHMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbNHNH);

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Chi tiết nhập hàng", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbNguyenLieuNH.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbNguyenLieuNH.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbNguyenLieuNHMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbNguyenLieuNH);

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1251, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        btnThemNH.setText("THÊM");
        btnThemNH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemNHActionPerformed(evt);
            }
        });

        btnThoatNH.setText("THOÁT");
        btnThoatNH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThoatNHActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnThemNH)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnThoatNH))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnThemNH)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnThoatNH)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(388, 388, 388)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Quản lý nhập hàng", jPanel1);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel2.setText("QUẢN LÝ MÓN ĂN");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(500, 500, 500)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Chi tiết món ăn", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbNguyenLieuMA.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tbNguyenLieuMA);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        tbMonAn.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbMonAn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbMonAnMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbMonAn);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 1263, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnThemMA.setText("THÊM");
        btnThemMA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemMAActionPerformed(evt);
            }
        });

        btnSuaMA.setText("SỬA");
        btnSuaMA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaMAActionPerformed(evt);
            }
        });

        btnXoaMA.setText("XÓA");
        btnXoaMA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaMAActionPerformed(evt);
            }
        });

        btnThoatMA.setText("THOÁT");
        btnThoatMA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThoatMAActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnThemMA)
                .addGap(107, 107, 107)
                .addComponent(btnSuaMA)
                .addGap(111, 111, 111)
                .addComponent(btnXoaMA)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnThoatMA)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnThemMA)
                    .addComponent(btnSuaMA)
                    .addComponent(btnXoaMA))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnThoatMA)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Quản lý món ăn", jPanel3);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel3.setText("QUẢN LÝ HÓA ĐƠN");

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(486, 486, 486))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel10Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel11.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Chi tiết hóa đơn", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbMonAnHD.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(tbMonAnHD);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        tbHoaDon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbHoaDon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbHoaDonMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(tbHoaDon);

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 1263, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnThemHD.setText("THÊM");
        btnThemHD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemHDActionPerformed(evt);
            }
        });

        btnThoatHD.setText("THOÁT");
        btnThoatHD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThoatHDActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnThemHD)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(1208, Short.MAX_VALUE)
                .addComponent(btnThoatHD)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnThemHD)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnThoatHD)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Quản lý hóa đơn", jPanel9);

        menuDX.setText("Đăng xuất");
        menuDX.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                menuDXMousePressed(evt);
            }
        });
        menuDX.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDXActionPerformed(evt);
            }
        });
        jMenuBar2.add(menuDX);

        menuDoiMK.setText("Đổi mật khẩu");
        menuDoiMK.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                menuDoiMKMousePressed(evt);
            }
        });
        menuDoiMK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuDoiMKActionPerformed(evt);
            }
        });
        jMenuBar2.add(menuDoiMK);

        setJMenuBar(jMenuBar2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 773, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnThoatNHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThoatNHActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnThoatNHActionPerformed

    private void tbNguyenLieuNHMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbNguyenLieuNHMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbNguyenLieuNHMouseClicked

    private void tbNHNHMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbNHNHMouseClicked

        defaultTMNH1.setRowCount(0);
        int row = tbNHNH.getSelectedRow();
        NhapHangDaiLY nh = nhapHangNHs.get(row);
        ctnnhs = ctnhBLL.getCTPNByMaPNNH(nh.getMaPNNH());

        for (ChiTietNhapDaiLy ctnh : ctnnhs) {
            nguyenLieuNH = nguyenLieuBLLNH.getNguyenLieuByMaNL(ctnh.getMaNL());
            loadTableDataNH(ctnh.getSoLuong(), ctnh.getTgVanChuyen());
        }
    }//GEN-LAST:event_tbNHNHMouseClicked

    private void tbMonAnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbMonAnMouseClicked
        // TODO add your handling code here:
        defaultTMMA1.setRowCount(0);
        int row = tbMonAn.getSelectedRow();
        SanPham chitietMA = monAns.get(row);
        nguonGocs = nguonGocBLL.getNguonGocbyMaMA(chitietMA.getMaMonAn());
        for (NguonGoc nguonGoc1 : nguonGocs) {
            nguyenLieuMA = nguyenLieuBLLMA.getNguyenLieuByMaNL(nguonGoc1.getMaNL());
            setTableDataNL();
        }
    }//GEN-LAST:event_tbMonAnMouseClicked

    private void btnThemMAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemMAActionPerformed
        // TODO add your handling code here:
        new ThemSanPhamView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnThemMAActionPerformed

    private void btnSuaMAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaMAActionPerformed
        // TODO add your handling code here:
        int row = tbMonAn.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(QLNhaHangView.this, "Vui lòng chọn món ăn trước", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            SanPham suaMonAn = monAns.get(row);
            if (maTaiKhoan.equals(suaMonAn.getMaNH())) {
                System.out.print("mã tài khoản" + maTaiKhoan + "mã nhà hàng" + suaMonAn.getMaNH());
                try {
                    new SuaMonSanPhamView(suaMonAn.getMaMonAn(), maTaiKhoan).setVisible(true);
                } catch (ParseException ex) {
                    java.util.logging.Logger.getLogger(SanPhamView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
                }
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(QLNhaHangView.this, "Món ăn này không thuộc nông trại của bạn. Vui lòng chọn món ăn khác!!!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnSuaMAActionPerformed

    private void btnXoaMAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaMAActionPerformed
        // TODO add your handling code here:
        int row = tbMonAn.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(QLNhaHangView.this, "Vui lòng chọn nguyên liệu trước", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            SanPham monAnXoa = monAns.get(row);
            if (maTaiKhoan.equals(monAnXoa.getMaNH())) {
                int confirm = JOptionPane.showConfirmDialog(QLNhaHangView.this, "Bạn có chắc chắn muốn xóa không?");

                if (confirm == JOptionPane.YES_OPTION) {
                    String maMonAn = String.valueOf(tbMonAn.getValueAt(row, 0));

                    nguonGocBLL.deleteNguonGocByMaMA(maMonAn);

                    monAnBLLMA.deleteMonAn(maMonAn);
                    monAns.remove(row);
                    defaultTMMA1.setRowCount(0);
                    defaultTMMA.setRowCount(0);
                    setTableDataMonAn(monAns);
                }
            } else {
                JOptionPane.showMessageDialog(QLNhaHangView.this, "Món ăn này không thuộc nông trại của bạn. Vui lòng chọn món ăn khác!!!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnXoaMAActionPerformed

    private void btnThoatMAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThoatMAActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnThoatMAActionPerformed

    private void tbHoaDonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbHoaDonMouseClicked
        // TODO add your handling code here:
        defaultTMHD1.setRowCount(0);
        int row = tbHoaDon.getSelectedRow();
        HoaDon hd = hoaDons.get(row);
        chiTietHoaDons = cthdBLL.getCTHDByMaHD(hd.getMaHD());
        for (ChiTietHoaDon cthd : chiTietHoaDons) {
            monAn = monAnBLLHD.getMonAnByMaMA(cthd.getMaMonAn());
            loadTableDataMAHD(cthd.getSoLuong());
        }
    }//GEN-LAST:event_tbHoaDonMouseClicked

    private void btnThemHDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemHDActionPerformed
        // TODO add your handling code here:
        new ThemHDView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnThemHDActionPerformed

    private void btnThoatHDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThoatHDActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnThoatHDActionPerformed

    private void btnThemNHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemNHActionPerformed
        // TODO add your handling code here:
        new ThemDaiLyView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnThemNHActionPerformed

    private void menuDXActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDXActionPerformed
        // TODO add your handling code here:
        new LogIn().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_menuDXActionPerformed

    private void menuDoiMKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuDoiMKActionPerformed
        // TODO add your handling code here:
        new DoiMatKhauView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_menuDoiMKActionPerformed

    private void menuDXMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuDXMousePressed
        // TODO add your handling code here:
        new LogIn().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_menuDXMousePressed

    private void menuDoiMKMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuDoiMKMousePressed
        // TODO add your handling code here:
        new DoiMatKhauView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_menuDoiMKMousePressed

//    public static void main(String args[]) {
//        new QLNhaHangView().setVisible(true);
//
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSuaMA;
    private javax.swing.JButton btnThemHD;
    private javax.swing.JButton btnThemMA;
    private javax.swing.JButton btnThemNH;
    private javax.swing.JButton btnThoatHD;
    private javax.swing.JButton btnThoatMA;
    private javax.swing.JButton btnThoatNH;
    private javax.swing.JButton btnXoaMA;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JMenu menuDX;
    private javax.swing.JMenu menuDoiMK;
    private javax.swing.JTable tbHoaDon;
    private javax.swing.JTable tbMonAn;
    private javax.swing.JTable tbMonAnHD;
    private javax.swing.JTable tbNHNH;
    private javax.swing.JTable tbNguyenLieuMA;
    private javax.swing.JTable tbNguyenLieuNH;
    // End of variables declaration//GEN-END:variables
}
