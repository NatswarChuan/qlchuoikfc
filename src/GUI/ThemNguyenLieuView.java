package GUI;

import BLL.ChungNhanBLL;
import BLL.NguyenLieuBLL;
import BLL.NongTraiBLL;
import BLL.TieuChuanBLL;
import DTO.ChungNhan;
import DTO.NguyenLieu;
import DTO.NongTrai;
import DTO.TieuChuan;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ThemNguyenLieuView extends javax.swing.JFrame {

    NguyenLieu nguyenLieu;
    NguyenLieuBLL nguyenLieuBLL;
    List<NguyenLieu> nguyenLieus;
    List<NongTrai> nongTrais;
    List<TieuChuan> tieuChuans;
    List<ChungNhan> chungNhans;
    NongTraiBLL nongTraiBLL;
    ChungNhanBLL chungNhanBLL;
    TieuChuanBLL tieuChuanBLL;

    String maTaiKhoan;

    public ThemNguyenLieuView(String maTK) {
        initComponents();
Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;

        nguyenLieu = new NguyenLieu();
        nguyenLieuBLL = new NguyenLieuBLL();
        nguyenLieus = new ArrayList<>();
        nguyenLieus = nguyenLieuBLL.getAllNguyenLieu();
        nongTraiBLL = new NongTraiBLL();
        chungNhanBLL = new ChungNhanBLL();
        tieuChuanBLL = new TieuChuanBLL();

        List<ChungNhan> chungNhans = chungNhanBLL.getAllChungNhan();
        for (ChungNhan chungNhan : chungNhans) {
            cbMaCN.addItem(chungNhan.getMaCN());
        }

        List<TieuChuan> tieuChuans = tieuChuanBLL.getAllChungNhan();
        for (TieuChuan tieuChuan : tieuChuans) {
            cbMaTC.addItem(tieuChuan.getMaTC());
        }

        nongTrais = nongTraiBLL.getAllNongTrai();
        for (NongTrai nt : nongTrais) {
            cbMaNT.addItem(nt.getMaNT());
        }

        txtMaNL.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    txtTenNL.requestFocus();
                }
            }
        });

        txtTenNL.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    cbMaNT.requestFocus();
                }
            }
        });

        cbMaNT.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    cbMaTC.requestFocus();
                }
            }
        });

        cbMaTC.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    cbMaCN.requestFocus();
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtMaNL = new javax.swing.JTextField();
        txtTenNL = new javax.swing.JTextField();
        btnThem = new javax.swing.JButton();
        lbNoti1 = new javax.swing.JLabel();
        lbNoti2 = new javax.swing.JLabel();
        lbNoti3 = new javax.swing.JLabel();
        lbNoti4 = new javax.swing.JLabel();
        lbNoti5 = new javax.swing.JLabel();
        lbNoti6 = new javax.swing.JLabel();
        lbNoti7 = new javax.swing.JLabel();
        lbNoti8 = new javax.swing.JLabel();
        lbNoti9 = new javax.swing.JLabel();
        lbNoti10 = new javax.swing.JLabel();
        cbMaNT = new javax.swing.JComboBox<>();
        cbMaTC = new javax.swing.JComboBox<>();
        cbMaCN = new javax.swing.JComboBox<>();
        btnTroLai = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel1.setText("THÊM NGUYÊN LIỆU");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(150, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(147, 147, 147))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Mã nguyên liệu:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Tên nguyên liệu:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Mã nông trại:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Mã tiêu chuẩn:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Mã chứng nhận:");

        btnThem.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnThem.setText("THÊM");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        lbNoti1.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti1.setText("   ");

        lbNoti2.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti2.setText("  ");

        lbNoti3.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti3.setText("  ");

        lbNoti4.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti4.setText("   ");

        lbNoti5.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti5.setText("   ");

        lbNoti6.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti6.setText("   ");

        lbNoti7.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti7.setText("   ");

        lbNoti8.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti8.setText("   ");

        lbNoti9.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti9.setText("   ");

        lbNoti10.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti10.setText("   ");

        cbMaNT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Chọn mã nông trại" }));
        cbMaNT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMaNTActionPerformed(evt);
            }
        });

        cbMaTC.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Chọn mã tiêu chuẩn" }));
        cbMaTC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMaTCActionPerformed(evt);
            }
        });

        cbMaCN.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Chọn mã chứng nhận" }));
        cbMaCN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMaCNActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addGap(70, 70, 70)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnThem)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(lbNoti2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lbNoti1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtMaNL, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                .addComponent(txtTenNL, javax.swing.GroupLayout.Alignment.LEADING))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(lbNoti6, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)))
                        .addComponent(lbNoti9, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lbNoti5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbNoti4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbMaTC, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbMaCN, 0, 200, Short.MAX_VALUE))
                        .addComponent(lbNoti10, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(lbNoti7, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                                    .addComponent(lbNoti8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(156, 156, 156))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(lbNoti3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(cbMaNT, javax.swing.GroupLayout.Alignment.LEADING, 0, 200, Short.MAX_VALUE))
                                .addGap(342, 342, 342)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtMaNL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(lbNoti1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTenNL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbMaNT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cbMaTC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cbMaCN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti5)
                .addGap(40, 40, 40)
                .addComponent(lbNoti6)
                .addGap(40, 40, 40)
                .addComponent(lbNoti7)
                .addGap(34, 34, 34)
                .addComponent(lbNoti8)
                .addGap(38, 38, 38)
                .addComponent(lbNoti9)
                .addGap(34, 34, 34)
                .addComponent(lbNoti10)
                .addGap(18, 18, 18)
                .addComponent(btnThem)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btnTroLai.setText("TRỞ LẠI");
        btnTroLai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroLaiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnTroLai)
                .addGap(39, 39, 39))
            .addGroup(layout.createSequentialGroup()
                .addGap(180, 180, 180)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(193, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(122, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(47, 47, 47)
                .addComponent(btnTroLai)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTroLaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroLaiActionPerformed
        // TODO add your handling code here:
        new NguyenLieuView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnTroLaiActionPerformed

    private void cbMaCNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbMaCNActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbMaCNActionPerformed

    private void cbMaTCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbMaTCActionPerformed
  
    }//GEN-LAST:event_cbMaTCActionPerformed

    private void cbMaNTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbMaNTActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_cbMaNTActionPerformed

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        // TODO add your handling code here:
        String maNL = txtMaNL.getText().trim();
        String tenNL = txtTenNL.getText().trim();
        String maNT = String.valueOf(cbMaNT.getSelectedItem());
        String maTC = String.valueOf(cbMaTC.getSelectedItem());
        String maCN = String.valueOf(cbMaCN.getSelectedItem());

        boolean check = true;

        if (maNL.equals("")) {
            lbNoti1.setText("Vui lòng nhập mã nguyên liệu!");
        } else {
            lbNoti1.setText(" ");
        }

        if (tenNL.equals("")) {
            lbNoti2.setText("Vui lòng nhập tên nguyên liệu!");
        } else {
            lbNoti2.setText(" ");
        }

        if (cbMaNT.getSelectedIndex() == 0) {
            lbNoti3.setText("Vui lòng chọn mã nông trại!");
        } else {
            lbNoti3.setText(" ");
        }

        if (cbMaTC.getSelectedIndex() == 0) {
            lbNoti4.setText("Vui lòng chọn mã tiêu chuẩn!");
        } else {
            lbNoti4.setText(" ");
        }

        if (cbMaCN.getSelectedIndex() == 0) {
            lbNoti5.setText("Vui lòng chọn mã chứng nhận!");
        } else {
            lbNoti5.setText(" ");
        }

        if (!maNL.equals("") && !tenNL.equals("") && (cbMaCN.getSelectedIndex() != 0) && (cbMaNT.getSelectedIndex() != 0) && (cbMaTC.getSelectedIndex() != 0)) {
            for (int i = 0; i < nguyenLieus.size(); i++) {
                if (nguyenLieus.get(i).getMaNL().equals(txtMaNL.getText())) {
                    lbNoti1.setText("Mã nguyên liệu đã tồn tại!");
                    check = false;
                    break;
                } else {
                    lbNoti1.setText("  ");
                }
            }

            if (check == true) {
                nguyenLieu.setMaNL(maNL);
                nguyenLieu.setTenNL(tenNL);
                nguyenLieu.setMaNT(maNT);
                nguyenLieu.setMaTC(maTC);
                nguyenLieu.setMaCN(maCN);
                nguyenLieuBLL.insertNguyenLieu(nguyenLieu);
                JOptionPane.showMessageDialog(ThemNguyenLieuView.this, "Thêm nguyên liệu thành công!", "Notification", JOptionPane.INFORMATION_MESSAGE);
                lbNoti10.setText(" ");
            } else {
                JOptionPane.showMessageDialog(ThemNguyenLieuView.this, "Thêm nguyên liệu thất bại!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnThemActionPerformed

//    public static void main(String args[]) {
//        //new ThemNguyenLieuView().setVisible(true);
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnTroLai;
    private javax.swing.JComboBox<String> cbMaCN;
    private javax.swing.JComboBox<String> cbMaNT;
    private javax.swing.JComboBox<String> cbMaTC;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbNoti1;
    private javax.swing.JLabel lbNoti10;
    private javax.swing.JLabel lbNoti2;
    private javax.swing.JLabel lbNoti3;
    private javax.swing.JLabel lbNoti4;
    private javax.swing.JLabel lbNoti5;
    private javax.swing.JLabel lbNoti6;
    private javax.swing.JLabel lbNoti7;
    private javax.swing.JLabel lbNoti8;
    private javax.swing.JLabel lbNoti9;
    private javax.swing.JTextField txtMaNL;
    private javax.swing.JTextField txtTenNL;
    // End of variables declaration//GEN-END:variables
}
