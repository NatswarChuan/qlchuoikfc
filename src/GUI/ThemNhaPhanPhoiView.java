package GUI;

import BLL.NhaPhanPhoiBLL;
import BLL.TaiKhoanBLL;
import DTO.NhaPhanPhoi;
import DTO.TaiKhoan;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.List;
import javax.swing.JOptionPane;

public class ThemNhaPhanPhoiView extends javax.swing.JFrame {

    NhaPhanPhoi nhaPhanPhoi;
    NhaPhanPhoiBLL nhaPhanPhoiBLL;
    List<NhaPhanPhoi> nhaPhanPhois;
    List<TaiKhoan> taiKhoans;
    TaiKhoanBLL taiKhoanBLL;
    
    String maTaiKhoan;
    
    public ThemNhaPhanPhoiView(String maTK) {
        initComponents();
        Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;
        
        nhaPhanPhoi = new NhaPhanPhoi();
        nhaPhanPhoiBLL = new NhaPhanPhoiBLL(); 
        nhaPhanPhois = nhaPhanPhoiBLL.getAllNhaPhanPhoi();
        
        taiKhoanBLL = new TaiKhoanBLL();
        taiKhoans = taiKhoanBLL.getAllTaiKhoan();
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        btnTroLai = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtMaNPP = new javax.swing.JTextField();
        txtTenNPP = new javax.swing.JTextField();
        txtDiaChi = new javax.swing.JTextField();
        txtSDT = new javax.swing.JTextField();
        txtMaTK = new javax.swing.JTextField();
        btnThem = new javax.swing.JButton();
        lbNoti1 = new javax.swing.JLabel();
        lbNoti2 = new javax.swing.JLabel();
        lbNoti3 = new javax.swing.JLabel();
        lbNoti4 = new javax.swing.JLabel();
        lbNoti5 = new javax.swing.JLabel();
        lbNoti6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnTroLai.setText("TRỞ LẠI");
        btnTroLai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroLaiActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("THÊM NHÀ PHÂN PHỐI");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(117, 117, 117)
                .addComponent(jLabel1)
                .addContainerGap(105, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Mã nhà phân phối:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Tên nhà phân phối:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Địa chỉ:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Số điện thoại:");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Mã tài khoản:");

        btnThem.setText("THÊM");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        lbNoti1.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti1.setText("    ");

        lbNoti2.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti2.setText("    ");

        lbNoti3.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti3.setText("   ");

        lbNoti4.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti4.setText("   ");

        lbNoti5.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti5.setText("   ");

        lbNoti6.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti6.setText("   ");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(85, 85, 85)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addGap(60, 60, 60)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbNoti5, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE)
                    .addComponent(lbNoti4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtDiaChi)
                    .addComponent(lbNoti2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnThem)
                    .addComponent(lbNoti1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTenNPP)
                    .addComponent(txtMaTK)
                    .addComponent(txtSDT)
                    .addComponent(lbNoti3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtMaNPP)
                    .addComponent(lbNoti6, javax.swing.GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE))
                .addContainerGap(65, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txtMaNPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtTenNPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti2)
                .addGap(49, 49, 49)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtDiaChi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSDT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMaTK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbNoti6)
                .addGap(18, 18, 18)
                .addComponent(btnThem)
                .addGap(31, 31, 31))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnTroLai)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTroLai)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        // TODO add your handling code here:
        String maNPP = txtMaNPP.getText().trim();
        String tenNPP = txtTenNPP.getText().trim();
        String diaChi = txtDiaChi.getText().trim();
        String sdt = txtSDT.getText().trim();
        String maTKNPP = txtMaTK.getText().trim();
        boolean check = true;
        
        if (maNPP.equals("")) {
            lbNoti1.setText("Vui lòng nhập mã nhà phân phối!");
        } else {
            lbNoti1.setText(" ");
        }
        
        if (tenNPP.equals("")) {
            lbNoti2.setText("Vui lòng nhập tên nhà phân phối!");
        } else {
            lbNoti2.setText(" ");
        }
        
        if (diaChi.equals("")) {
            lbNoti3.setText("Vui lòng nhập địa chỉ!");
        } else {
            lbNoti3.setText(" ");
        }
        
        if (sdt.equals("")) {
            lbNoti4.setText("Vui lòng nhập số điện thoại!");
        } else {
            lbNoti4.setText(" ");
        }
        
        if (maTKNPP.equals("")) {
            lbNoti5.setText("Vui lòng nhập mã tài khoản!");
        } else {
            lbNoti5.setText(" ");
        }
        
        // chưa bắt lỗi trùng mã tài khoản, trùng mã nhà phân phối
        if ((!maNPP.equals("")) && (!tenNPP.equals("")) && (!diaChi.equals("")) && (!sdt.equals("")) && (!maTKNPP.equals(""))) {
            nhaPhanPhoi.setMaNPP(maNPP);
            nhaPhanPhoi.setTenNPP(tenNPP);
            nhaPhanPhoi.setDiaChi(diaChi);
            nhaPhanPhoi.setSDT(sdt);
            nhaPhanPhoi.setMaTK(maTKNPP);
            
            for (int i = 0; i < nhaPhanPhois.size(); i++) {
                if (nhaPhanPhois.get(i).getMaNPP().equals(txtMaNPP.getText())) {
                    lbNoti1.setText("Mã nhà phân phối đã tồn tại!");
                    check = false;
                    break;
                } else {
                    lbNoti1.setText("  ");
                }
            }
            
            for (int i = 0; i < taiKhoans.size(); i++) {
                if (!taiKhoans.get(i).getMaTK().equals(txtMaTK.getText())) {
                    lbNoti5.setText("Mã tài khoản không tồn tại!");
                    lbNoti6.setText("Vui lòng tạo tài khoản và thử lại.");
                    check = false;
                } else {
                    check = true;
                    lbNoti6.setText(" ");
                    lbNoti5.setText(" ");
                    break;
                }
            }
            
            if (check == true) {
                
                nhaPhanPhois.add(nhaPhanPhoi);
                nhaPhanPhoiBLL.insertNhaPhanPhoi(nhaPhanPhoi);
                lbNoti1.setText(" ");
                lbNoti2.setText(" ");
                lbNoti3.setText(" ");
                lbNoti4.setText(" ");
                lbNoti5.setText(" ");

                JOptionPane.showMessageDialog(ThemNhaPhanPhoiView.this, "Thêm nhà phân phối thành công!", "Notification", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(ThemNhaPhanPhoiView.this, "Thêm nhà phân phối thất bại!", "Error", JOptionPane.ERROR_MESSAGE);           
            } 
            
        }
             
    }//GEN-LAST:event_btnThemActionPerformed

    private void btnTroLaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroLaiActionPerformed
        // TODO add your handling code here:
        new NhaPhanPhoiView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnTroLaiActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(ThemNhaPhanPhoiView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(ThemNhaPhanPhoiView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(ThemNhaPhanPhoiView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(ThemNhaPhanPhoiView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new ThemNhaPhanPhoiView().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnTroLai;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel lbNoti1;
    private javax.swing.JLabel lbNoti2;
    private javax.swing.JLabel lbNoti3;
    private javax.swing.JLabel lbNoti4;
    private javax.swing.JLabel lbNoti5;
    private javax.swing.JLabel lbNoti6;
    private javax.swing.JTextField txtDiaChi;
    private javax.swing.JTextField txtMaNPP;
    private javax.swing.JTextField txtMaTK;
    private javax.swing.JTextField txtSDT;
    private javax.swing.JTextField txtTenNPP;
    // End of variables declaration//GEN-END:variables
}
