package GUI;

import BLL.NguyenLieuBLL;
import BLL.NongTraiBLL;
import BLL.TaiKhoanBLL;
import DTO.NguyenLieu;
import DTO.NongTrai;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class NguyenLieuView extends javax.swing.JFrame {

    NguyenLieu nguyenLieu;
    NguyenLieuBLL nguyenLieuBLL;
    List<NguyenLieu> nguyenLieus;
    
    DefaultTableModel defaultTableModel;
    
    String maTaiKhoan;
    
    public NguyenLieuView(String maTK) {
        initComponents();
        Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;
        
        System.out.println(maTaiKhoan);
        
        nguyenLieu = new NguyenLieu();
        nguyenLieuBLL = new NguyenLieuBLL();
        nguyenLieus = new ArrayList<>();
        
        nguyenLieus = nguyenLieuBLL.getAllNguyenLieu();
                
        defaultTableModel = new DefaultTableModel(){
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
         
        tbNongSan.setModel(defaultTableModel);
        
        defaultTableModel.addColumn("MÃ NGUYÊN LIỆU");
        defaultTableModel.addColumn("TÊN NGUYÊN LIỆU");
        defaultTableModel.addColumn("MÃ NÔNG TRẠI");
        defaultTableModel.addColumn("MÃ TIÊU CHUẨN");
        defaultTableModel.addColumn("MÃ CHỨNG NHẬN");     
        
        setTableData(nguyenLieus);
    }

    private void setTableData(List<NguyenLieu> nguyenLieus){
        for(NguyenLieu nguyenLieu : nguyenLieus){
            defaultTableModel.addRow(new Object[]{nguyenLieu.getMaNL(), nguyenLieu.getTenNL(), nguyenLieu.getMaNT(), nguyenLieu.getMaTC(), nguyenLieu.getMaCN()});
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbNongSan = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        btnThem = new javax.swing.JButton();
        btnSua = new javax.swing.JButton();
        btnXoa = new javax.swing.JButton();
        btnThoat = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        menuDX = new javax.swing.JMenu();
        menuDoiMK = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel1.setText("QUẢN LÝ NGUYÊN LIỆU");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(407, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(387, 387, 387))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tbNongSan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbNongSan);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 463, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        btnThem.setText("THÊM");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        btnSua.setText("SỬA");
        btnSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaActionPerformed(evt);
            }
        });

        btnXoa.setText("XÓA");
        btnXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaActionPerformed(evt);
            }
        });

        btnThoat.setText("THOÁT");
        btnThoat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThoatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(btnThem)
                .addGap(88, 88, 88)
                .addComponent(btnSua)
                .addGap(112, 112, 112)
                .addComponent(btnXoa)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnThoat)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnThem)
                    .addComponent(btnSua)
                    .addComponent(btnXoa))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnThoat)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        menuDX.setText("Đăng xuất");
        menuDX.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                menuDXMousePressed(evt);
            }
        });
        jMenuBar1.add(menuDX);

        menuDoiMK.setText("Đổi mật khẩu");
        menuDoiMK.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                menuDoiMKKeyPressed(evt);
            }
        });
        jMenuBar1.add(menuDoiMK);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        // TODO add your handling code here:
        new ThemNguyenLieuView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnThemActionPerformed

    private void btnSuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaActionPerformed
        // TODO add your handling code here:
        int row = tbNongSan.getSelectedRow();
        if (row == -1) {
            JOptionPane.showMessageDialog(NguyenLieuView.this, "Vui lòng chọn nguyên liệu trước", "Error", JOptionPane.ERROR_MESSAGE);
        }
        else {
            NguyenLieu suaNL = nguyenLieus.get(row);
            NongTraiBLL tkbll = new NongTraiBLL();
            NongTrai nt = tkbll.getNongTraiByMaNT(suaNL.getMaNT());
            if (maTaiKhoan.equals(nt.getMaTK())) {
                try {  
                    new SuaNguyenLieuView(suaNL.getMaNL(), maTaiKhoan).setVisible(true);
                } catch (ParseException ex) {
                    Logger.getLogger(NguyenLieuView.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(NguyenLieuView.this, "Nguyên liệu này không thuộc nông trại của bạn. Vui lòng chọn nguyên liệu khác!!!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnSuaActionPerformed

    private void btnXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaActionPerformed
        // TODO add your handling code here:
        int row = tbNongSan.getSelectedRow();
        if(row == -1){
            JOptionPane.showMessageDialog(NguyenLieuView.this, "Vui lòng chọn nguyên liệu trước", "Error", JOptionPane.ERROR_MESSAGE);
        }
        else{
            NguyenLieu suaNL = nguyenLieus.get(row); 
            NongTraiBLL tkbll = new NongTraiBLL();
            NongTrai nt = tkbll.getNongTraiByMaNT(suaNL.getMaNT());
            if (maTaiKhoan.equals(nt.getMaTK())) {
                int confirm = JOptionPane.showConfirmDialog(NguyenLieuView.this,"Bạn có chắc chắn muốn xóa không?");

                if(confirm == JOptionPane.YES_OPTION){
                    String maNL = String.valueOf(tbNongSan.getValueAt(row, 0));

                    nguyenLieuBLL.deleteNguyenLieu(maNL);
                    nguyenLieus.remove(row);
                    defaultTableModel.setRowCount(0);
                    setTableData(nguyenLieus);
                }
            } else {
                JOptionPane.showMessageDialog(NguyenLieuView.this, "Nguyên liệu này không thuộc nông trại của bạn. Vui lòng chọn nguyên liệu khác!!!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btnXoaActionPerformed

    private void btnThoatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThoatActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnThoatActionPerformed

    private void menuDXMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_menuDXMousePressed
        // TODO add your handling code here:
         new LogIn().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_menuDXMousePressed

    private void menuDoiMKKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_menuDoiMKKeyPressed
        // TODO add your handling code here:
//        new DoiMatKhauView(maTaiKhoan).setVisible(true);
//        this.dispose();
    }//GEN-LAST:event_menuDoiMKKeyPressed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSua;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnThoat;
    private javax.swing.JButton btnXoa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenu menuDX;
    private javax.swing.JMenu menuDoiMK;
    private javax.swing.JTable tbNongSan;
    // End of variables declaration//GEN-END:variables
}
