package GUI;

import BLL.ChiTietNhapDLBLL;
import BLL.NguyenLieuBLL;
import BLL.NhapHangDLBLL;
import DTO.ChiTietNhapDaiLy;
import DTO.NguyenLieu;
import DTO.NhapHangDaiLY;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;

public class NhapHangNHView extends javax.swing.JFrame {

    List<NhapHangDaiLY> nhapHangNHs;
    NhapHangDLBLL nhapHangNHBLL;
    ChiTietNhapDaiLy chiTietNhapHangNH;
    ChiTietNhapDLBLL ctnhBLL;
    List<ChiTietNhapDaiLy> ctnnhs;
    NguyenLieu nguyenLieu;
    NguyenLieuBLL nguyenLieuBLL;
    
    DefaultTableModel defaultTableModel, defaultTableModel1;
    
    String maTaiKhoan;
    
    public NhapHangNHView(String maTK) {
        initComponents();
        Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;
        
        nhapHangNHs = new ArrayList<>();
        nhapHangNHBLL = new NhapHangDLBLL();
        chiTietNhapHangNH = new ChiTietNhapDaiLy();
        ctnhBLL = new ChiTietNhapDLBLL();
        ctnnhs = new ArrayList<>();
        nguyenLieu = new NguyenLieu();
        nguyenLieuBLL = new NguyenLieuBLL();
        
        nhapHangNHs = nhapHangNHBLL.getAllNhapHangNHs();
        
        
        defaultTableModel = new DefaultTableModel(){
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        tbNHNH.setModel(defaultTableModel);
        defaultTableModel.addColumn("MÃ PHIẾU NHẬP NH");
        defaultTableModel.addColumn("MÃ NHÀ PHÂN PHỐI");
        defaultTableModel.addColumn("THỜI GIAN NHẬP");
        defaultTableModel.addColumn("MÃ NHÀ HÀNG");  
        
        setTableDataNhapHang(nhapHangNHs);
        
        defaultTableModel1 = new DefaultTableModel(){
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
         
        tbNguyenLieu.setModel(defaultTableModel1);        
        defaultTableModel1.addColumn("MÃ NGUYÊN LIỆU");
        defaultTableModel1.addColumn("TÊN NGUYÊN LIỆU");
        defaultTableModel1.addColumn("MÃ NÔNG TRẠI");
        defaultTableModel1.addColumn("MÃ TIÊU CHUẨN");
        defaultTableModel1.addColumn("MÃ CHỨNG NHẬN");
        defaultTableModel1.addColumn("HTGS");
        defaultTableModel1.addColumn("HTCT");
        defaultTableModel1.addColumn("TG THU HOẠCH");
        defaultTableModel1.addColumn("TG SƠ CHẾ");
        defaultTableModel1.addColumn("TG ĐÓNG GÓI");
        defaultTableModel1.addColumn("KHỐI LƯỢNG");
        defaultTableModel1.addColumn("TG VẬN CHUYỂN");
    }
    
    private void setTableDataNhapHang(List<NhapHangDaiLY> nhapHangNHs){
        for(NhapHangDaiLY nhapHangNH : nhapHangNHs){
            defaultTableModel.addRow(new Object[]{nhapHangNH.getMaPNNH(), nhapHangNH.getMaNPP(), nhapHangNH.getTgNhap(), nhapHangNH.getMaNH()});
        }
    }

    private void loadTableData(int klg, String tgvc){
        DefaultTableModel model = (DefaultTableModel) tbNguyenLieu.getModel();
                
        model.addRow(new Object[]{nguyenLieu.getMaNL(), nguyenLieu.getTenNL(), nguyenLieu.getMaNT(), nguyenLieu.getMaTC(), nguyenLieu.getMaCN(), klg, tgvc
        });
        
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        itemXoa = new javax.swing.JMenuItem();
        jPanel3 = new javax.swing.JPanel();
        btnThem = new javax.swing.JButton();
        btnLamMoi = new javax.swing.JButton();
        btnThoat = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbNguyenLieu = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbNHNH = new javax.swing.JTable();

        itemXoa.setText("Xóa");
        itemXoa.setToolTipText("");
        jPopupMenu1.add(itemXoa);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        btnThem.setText("THÊM");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        btnLamMoi.setText("LÀM MỚI");

        btnThoat.setText("THOÁT");
        btnThoat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThoatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnThem)
                .addGap(107, 107, 107)
                .addComponent(btnLamMoi)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnThoat)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnThem)
                    .addComponent(btnLamMoi))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnThoat)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel1.setText("QUẢN LÝ NHẬP HÀNG NHÀ HÀNG");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(377, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(373, 373, 373))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Chi tiết nhập hàng", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbNguyenLieu.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbNguyenLieu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbNguyenLieuMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbNguyenLieu);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 10, Short.MAX_VALUE))
        );

        tbNHNH.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbNHNH.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbNHNHMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbNHNH);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        // TODO add your handling code here:
        new ThemDaiLyView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnThemActionPerformed

    private void btnThoatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThoatActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnThoatActionPerformed

    private void tbNHNHMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbNHNHMouseClicked
        // TODO add your handling code here:
        defaultTableModel1.setRowCount(0);
        int row = tbNHNH.getSelectedRow();
        NhapHangDaiLY nh = nhapHangNHs.get(row);
        ctnnhs = ctnhBLL.getCTPNByMaPNNH(nh.getMaPNNH());
        
        for (ChiTietNhapDaiLy ctnh : ctnnhs) {
            nguyenLieu = nguyenLieuBLL.getNguyenLieuByMaNL(ctnh.getMaNL());
            loadTableData(ctnh.getSoLuong(), ctnh.getTgVanChuyen());
        }
    }//GEN-LAST:event_tbNHNHMouseClicked

    private void tbNguyenLieuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbNguyenLieuMouseClicked
        // TODO add your handling code here:
        
    }//GEN-LAST:event_tbNguyenLieuMouseClicked

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        new NhapHangNHView().setVisible(true);
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnLamMoi;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnThoat;
    private javax.swing.JMenuItem itemXoa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable tbNHNH;
    private javax.swing.JTable tbNguyenLieu;
    // End of variables declaration//GEN-END:variables
}
