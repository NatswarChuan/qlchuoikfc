package GUI;

import BLL.ChiTietNhapDLBLL;
import BLL.NguyenLieuBLL;
import BLL.NhaPhanPhoiBLL;
import BLL.NhapHangDLBLL;
import DTO.ChiTietNhapDaiLy;
import DTO.NguyenLieu;
import DTO.NhaPhanPhoi;
import DTO.NhapHangDaiLY;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ThemDaiLyView extends javax.swing.JFrame {

    List<NguyenLieu> nguyenLieus;
    NguyenLieu nguyenLieu;
    NguyenLieuBLL nguyenLieuBLL;
    NhapHangDaiLY nhapHangNH;
    NhapHangDLBLL nhapHangNHBLL;
    List<NhapHangDaiLY> nhapHangNHs;
    ChiTietNhapDaiLy chiTietNhapNH;
    List<ChiTietNhapDaiLy> ctnhs;
    ChiTietNhapDLBLL ctnhBLL;
    NhaPhanPhoi nhaPhanPhoi;
    NhaPhanPhoiBLL nhaPhanPhoiBLL;
    List<NhaPhanPhoi> nhaPhanPhois;
    String tgvc_format, tgn_format;
    boolean checkTime = true;

    DefaultTableModel defaultTableModel;

    String maTaiKhoan;

    public ThemDaiLyView(String maTK) {
        initComponents();
Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;

        nguyenLieu = new NguyenLieu();
        nguyenLieus = new ArrayList<>();
        nguyenLieuBLL = new NguyenLieuBLL();
        nhapHangNH = new NhapHangDaiLY();
        nhapHangNHBLL = new NhapHangDLBLL();
        ctnhs = new ArrayList<>();
        ctnhBLL = new ChiTietNhapDLBLL();
        nhaPhanPhoi = new NhaPhanPhoi();
        nhaPhanPhoiBLL = new NhaPhanPhoiBLL();
        nhaPhanPhois = new ArrayList<>();
        nhapHangNHs = new ArrayList<>();

        nhapHangNHs = nhapHangNHBLL.getAllNhapHangNHs();

        tbNguyenLieu.setComponentPopupMenu(popUpMenuNL);

        txtMaNH.setText(maTK);

        List<NguyenLieu> nguyenLieus = nguyenLieuBLL.getAllNguyenLieu();
        for (NguyenLieu nguyenLieu : nguyenLieus) {
            cbNguyenLieu.addItem(nguyenLieu.getMaNL());
        }

        List<NhaPhanPhoi> nhaPhanPhois = nhaPhanPhoiBLL.getAllNhaPhanPhoi();
        for (NhaPhanPhoi nhaPP : nhaPhanPhois) {
            cbMaNPP.addItem(nhaPP.getMaNPP());
        }

        try {
            tgn_format = nhapHangNH.formatDateString(chooseDateTGNhap.getDate());
        } catch (ParseException ex) {

        }

        defaultTableModel = new DefaultTableModel() {
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tbNguyenLieu.setModel(defaultTableModel);

        defaultTableModel.addColumn("MÃ NGUYÊN LIỆU");
        defaultTableModel.addColumn("TÊN NGUYÊN LIỆU");
        defaultTableModel.addColumn("MÃ NÔNG TRẠI");
        defaultTableModel.addColumn("MÃ TIÊU CHUẨN");
        defaultTableModel.addColumn("MÃ CHỨNG NHẬN");
        defaultTableModel.addColumn("HTGS");
        defaultTableModel.addColumn("HTCT");
        defaultTableModel.addColumn("TG THU HOẠCH");
        defaultTableModel.addColumn("TG SƠ CHẾ");
        defaultTableModel.addColumn("TG ĐÓNG GÓI");
        defaultTableModel.addColumn("KHỐI LƯỢNG");
        defaultTableModel.addColumn("TG VẬN CHUYỂN");
    }

    // thêm xóa nguyên liệu trong bảng, XÓA HÀNG TRONG BẢNG KHI NHẬP SAI THỜI GIAN
    private void loadTableData(int klg, String tgvc) {
        DefaultTableModel model = (DefaultTableModel) tbNguyenLieu.getModel();

        model.addRow(new Object[]{nguyenLieu.getMaNL(), nguyenLieu.getTenNL(), nguyenLieu.getMaNT(), nguyenLieu.getMaTC(), nguyenLieu.getMaCN(), klg, tgvc
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popUpMenuNL = new javax.swing.JPopupMenu();
        itemXoa = new javax.swing.JMenuItem();
        jPanel12 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbNguyenLieu = new javax.swing.JTable();
        btnThem = new javax.swing.JButton();
        btnThoat = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        cbNguyenLieu = new javax.swing.JComboBox<>();
        jLabel23 = new javax.swing.JLabel();
        btnChon = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        chooseTGVC = new com.toedter.calendar.JDateChooser();
        lbNoti3 = new javax.swing.JLabel();
        spinnerSL = new javax.swing.JSpinner();
        lbNoti4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtTenNL = new javax.swing.JTextField();
        jPanel15 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        txtMaPN = new javax.swing.JTextField();
        txtMaNH = new javax.swing.JTextField();
        lbNoti1 = new javax.swing.JLabel();
        lbCheckmaNV = new javax.swing.JLabel();
        chooseDateTGNhap = new com.toedter.calendar.JDateChooser();
        cbMaNPP = new javax.swing.JComboBox<>();
        lbNoti2 = new javax.swing.JLabel();
        txtTenNPP = new javax.swing.JTextField();

        itemXoa.setText("Xóa");
        itemXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemXoaActionPerformed(evt);
            }
        });
        popUpMenuNL.add(itemXoa);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel26.setText("THÊM NHẬP HÀNG NHÀ HÀNG");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addGap(356, 356, 356)
                .addComponent(jLabel26)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel26)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Danh sách nguyên liệu", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbNguyenLieu.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "", "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbNguyenLieu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbNguyenLieuMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbNguyenLieu);

        btnThem.setText("Thêm");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        btnThoat.setBackground(new java.awt.Color(204, 255, 153));
        btnThoat.setText("Trở lại");
        btnThoat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThoatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(474, 474, 474)
                .addComponent(btnThem)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 1098, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnThoat)))
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnThem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnThoat)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Chi tiết phiếu nhập", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel21.setText("Mã nguyên liệu: ");

        cbNguyenLieu.setMaximumRowCount(50);
        cbNguyenLieu.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Chọn nguyên liệu" }));
        cbNguyenLieu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbNguyenLieuActionPerformed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel23.setText("Khối lượng mua: ");

        btnChon.setBackground(new java.awt.Color(204, 204, 204));
        btnChon.setText("Chọn");
        btnChon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChonActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("TG vận chuyển:");

        chooseTGVC.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chooseTGVCMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                chooseTGVCMouseEntered(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                chooseTGVCMousePressed(evt);
            }
        });

        lbNoti3.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti3.setText("   ");

        spinnerSL.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        spinnerSL.setModel(new javax.swing.SpinnerNumberModel(10, 10, 10000, 10));
        spinnerSL.setMinimumSize(new java.awt.Dimension(80, 22));

        lbNoti4.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti4.setText("   ");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("kg");

        txtTenNL.setEditable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel21)
                    .addComponent(jLabel23)
                    .addComponent(jLabel1))
                .addGap(34, 34, 34)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(cbNguyenLieu, 0, 144, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTenNL, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(spinnerSL, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lbNoti3, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(chooseTGVC, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(lbNoti4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(210, 210, 210)
                .addComponent(btnChon)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(cbNguyenLieu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTenNL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(lbNoti3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel23)
                    .addComponent(spinnerSL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(chooseTGVC, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti4)
                .addGap(10, 10, 10)
                .addComponent(btnChon)
                .addGap(20, 20, 20))
        );

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin chung", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel29.setText("Mã phiếu nhập NH: ");

        jLabel30.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel30.setText("Mã nhà hàng:");

        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel31.setText("Mã nhà phân phối:");

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel32.setText("Thời gian nhập:");

        txtMaPN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMaPNActionPerformed(evt);
            }
        });

        txtMaNH.setEditable(false);
        txtMaNH.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMaNHActionPerformed(evt);
            }
        });

        lbNoti1.setForeground(new java.awt.Color(255, 51, 0));

        lbCheckmaNV.setForeground(new java.awt.Color(255, 51, 0));

        cbMaNPP.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Chọn nhà phân phối" }));
        cbMaNPP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMaNPPActionPerformed(evt);
            }
        });

        lbNoti2.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti2.setText("   ");

        txtTenNPP.setEditable(false);

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel31)
                    .addComponent(jLabel29)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addComponent(lbCheckmaNV, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel32)
                    .addComponent(jLabel30))
                .addGap(18, 18, 18)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lbNoti2, javax.swing.GroupLayout.DEFAULT_SIZE, 200, Short.MAX_VALUE)
                            .addComponent(txtMaNH)
                            .addComponent(lbNoti1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtMaPN)
                            .addComponent(chooseDateTGNhap, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(cbMaNPP, 0, 169, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTenNPP, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(txtMaPN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addComponent(lbNoti1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(cbMaNPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTenNPP, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti2)
                .addGap(11, 11, 11)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addComponent(jLabel32)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbCheckmaNV, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(chooseDateTGNhap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(txtMaNH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(54, 54, 54))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tbNguyenLieuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbNguyenLieuMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbNguyenLieuMouseClicked

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        boolean check = true;

        String maPNNH = txtMaPN.getText().trim();
        String maNPP = String.valueOf(cbMaNPP.getSelectedItem());

        if (maPNNH.equals("")) {
            lbNoti1.setText("Vui lòng nhập mã phiếu nhập!");
        }

        if (cbNguyenLieu.getSelectedIndex() == 0) {
            lbNoti3.setText("Vui lòng chọn mã nguyên liệu!");
            txtTenNL.setText(" ");
        }

        if (cbMaNPP.getSelectedIndex() == 0) {
            lbNoti2.setText("Chọn mã chứng nhận!");
            txtTenNPP.setText(" ");
        }

        if ((tgvc_format.compareTo(tgn_format) >= 0)) {
            System.out.println("check time = " + tgvc_format.compareTo(tgn_format));
            checkTime = false;
            lbNoti4.setText("TG vận chuyển phải trước TG nhập!");
        }

        if ((!maPNNH.equals("")) && (cbMaNPP.getSelectedIndex() != 0) && checkTime == true) {
            for (int i = 0; i < nhapHangNHs.size(); i++) {
                if (nhapHangNHs.get(i).getMaPNNH().equals(txtMaPN.getText())) {
                    lbNoti1.setText("Trùng mã phiếu nhập!");
                    check = false;
                    break;
                }
            }

            if (check == true) {
                nhapHangNH.setMaPNNH(maPNNH);
                nhapHangNH.setMaNPP(String.valueOf(cbMaNPP.getSelectedItem()));
                nhapHangNH.setTgNhap(tgn_format);

                // Phải đúng mã nhà hàng, do là khóa ngoại
                nhapHangNH.setMaNH(maTaiKhoan);

                nhapHangNHBLL.insertNhapHangNH(nhapHangNH);

                for (ChiTietNhapDaiLy ctnnh : ctnhs) {
                    ctnhBLL.insertCTPN(ctnnh);
                }

                JOptionPane.showMessageDialog(ThemDaiLyView.this, "Thêm phiếu nhập thành công!", "Notification", JOptionPane.INFORMATION_MESSAGE);

                lbNoti1.setText(" ");
                lbNoti2.setText(" ");
                lbNoti3.setText(" ");
                lbNoti4.setText(" ");
                ctnhs = new ArrayList<>();
                txtMaPN.setText("");
                defaultTableModel.setRowCount(0);
            }

        } else {
            JOptionPane.showMessageDialog(ThemDaiLyView.this, "Thêm phiểu nhập thất bại!", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnThemActionPerformed

    private void btnThoatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThoatActionPerformed
        // TODO add your handling code here:
        new QLNhaHangView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnThoatActionPerformed

    private void txtMaPNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMaPNActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaPNActionPerformed

    private void txtMaNHActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMaNHActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaNHActionPerformed

    private void cbNguyenLieuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbNguyenLieuActionPerformed
        // TODO add your handling code here:
        if (cbNguyenLieu.getSelectedIndex() == 0) {
            lbNoti3.setText("Vui lòng chọn mã nguyên liệu!");
            txtTenNL.setText(" ");
        } else {
            nguyenLieu = nguyenLieuBLL.getNguyenLieuByMaNL(String.valueOf(cbNguyenLieu.getSelectedItem()));
            txtTenNL.setText(nguyenLieu.getTenNL());
            lbNoti3.setText(" ");
        }
    }//GEN-LAST:event_cbNguyenLieuActionPerformed

    private void btnChonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChonActionPerformed
        // TODO add your handling code here:
        boolean check = true;
        tgvc_format = null;
        checkTime = true;

        if (cbNguyenLieu.getSelectedIndex() != 0) {

            Date tg_vc = chooseTGVC.getDate();
            chiTietNhapNH = new ChiTietNhapDaiLy();

            try { // bắt lỗi thời gian vận chuyển trước thời gian nhập
                tgvc_format = chiTietNhapNH.formatDateString(tg_vc);
                chiTietNhapNH.setTgVanChuyen(tgvc_format);

            } catch (ParseException ex) {

            }

            if ((tgvc_format.compareTo(tgn_format) >= 0)) {
                System.out.println("check time = " + tgvc_format.compareTo(tgn_format));
                checkTime = false;
                lbNoti4.setText("TG vận chuyển phải trước TG nhập!");
            }

            chiTietNhapNH.setMaPNNH(txtMaPN.getText());
            int sl = (Integer) spinnerSL.getValue();
            chiTietNhapNH.setSoLuong(sl);
            chiTietNhapNH.setMaNL(String.valueOf(cbNguyenLieu.getSelectedItem()));

            for (int i = 0; i < ctnhs.size(); i++) {
                if (ctnhs.get(i).getMaNL().equals(chiTietNhapNH.getMaNL())) {
                    lbNoti3.setText("Nguyên liệu đã tồn tại!");
                    check = false;
                    break;
                }
            }

            if (check == true && checkTime == true) {
                loadTableData(sl, tgvc_format);
                ctnhs.add(chiTietNhapNH);
                lbNoti4.setText("  ");
            }

        } else {
            lbNoti3.setText("Vui lòng chọn mã nguyên liệu!");
            txtTenNL.setText(" ");
        }
    }//GEN-LAST:event_btnChonActionPerformed

    private void cbMaNPPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbMaNPPActionPerformed
        // TODO add your handling code here:
        if (cbMaNPP.getSelectedIndex() == 0) {
            lbNoti2.setText("Vui lòng chọn mã nhà phân phối!");
            txtTenNPP.setText(" ");
        } else {
            nhaPhanPhoi = nhaPhanPhoiBLL.getNPPByMaNPP(String.valueOf(cbMaNPP.getSelectedItem()));
            txtTenNPP.setText(nhaPhanPhoi.getTenNPP());
            lbNoti2.setText(" ");
        }
    }//GEN-LAST:event_cbMaNPPActionPerformed

    private void chooseTGVCMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chooseTGVCMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_chooseTGVCMouseClicked

    private void chooseTGVCMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chooseTGVCMousePressed

    }//GEN-LAST:event_chooseTGVCMousePressed

    private void chooseTGVCMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chooseTGVCMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_chooseTGVCMouseEntered

    private void itemXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemXoaActionPerformed
        // TODO add your handling code here:
        int i = tbNguyenLieu.getSelectedRow();

        defaultTableModel.removeRow(i);
        ctnhs.remove(ctnhs.get(i));
    }//GEN-LAST:event_itemXoaActionPerformed

    /**
     * @param args the command line arguments
     */
//    public static void main(String args[]) {
//        new ThemNHNHView().setVisible(true);
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChon;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnThoat;
    private javax.swing.JComboBox<String> cbMaNPP;
    private javax.swing.JComboBox<String> cbNguyenLieu;
    private com.toedter.calendar.JDateChooser chooseDateTGNhap;
    private com.toedter.calendar.JDateChooser chooseTGVC;
    private javax.swing.JMenuItem itemXoa;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lbCheckmaNV;
    private javax.swing.JLabel lbNoti1;
    private javax.swing.JLabel lbNoti2;
    private javax.swing.JLabel lbNoti3;
    private javax.swing.JLabel lbNoti4;
    private javax.swing.JPopupMenu popUpMenuNL;
    private javax.swing.JSpinner spinnerSL;
    private javax.swing.JTable tbNguyenLieu;
    private javax.swing.JTextField txtMaNH;
    private javax.swing.JTextField txtMaPN;
    private javax.swing.JTextField txtTenNL;
    private javax.swing.JTextField txtTenNPP;
    // End of variables declaration//GEN-END:variables
}
