package GUI;

import BLL.ChiTietNhapNPPBLL;
import BLL.NguyenLieuBLL;
import BLL.NhaPhanPhoiBLL;
import BLL.NhapHangDLPBLL;
import BLL.NongTraiBLL;
import DTO.ChiTietNhapNPP;
import DTO.NguyenLieu;
import DTO.NhaPhanPhoi;
import DTO.NhapHangNPP;
import DTO.NongTrai;
import java.awt.Image;
import java.awt.Toolkit;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ThemNHNPPView extends javax.swing.JFrame {

    List<NguyenLieu> nguyenLieus;
    NguyenLieu nguyenLieu;
    NguyenLieuBLL nguyenLieuBLL;
    NhaPhanPhoi nhaPhanPhoi;
    NhaPhanPhoiBLL nppbll;
    ChiTietNhapNPP chiTietNhapNPP;
    List<ChiTietNhapNPP> ctnhs;
    ChiTietNhapNPPBLL ctnnppbll;
    List<NhaPhanPhoi> nhaPhanPhois;
    List<NhapHangNPP> hangNPPs;
    NhapHangNPP nhapHangNPP;
    NhapHangDLPBLL nhapHangNPPBLL;
    NongTrai nongTrai;
    NongTraiBLL nongTraiBLL;
    List<NongTrai> nongTrais;
    String tgvc_format, tgn_format;

    DefaultTableModel defaultTableModel;

    String maTaiKhoan;

    public ThemNHNPPView(String maTK) {
        initComponents();
Image icon = Toolkit.getDefaultToolkit().getImage("./src/image/vinamilk.png");
        this.setIconImage(icon);
        this.setLayout(null);
        this.setVisible(true);
        maTaiKhoan = maTK;

        nguyenLieus = new ArrayList<>();
        nguyenLieu = new NguyenLieu();
        nguyenLieuBLL = new NguyenLieuBLL();
        nhaPhanPhoi = new NhaPhanPhoi();
        nppbll = new NhaPhanPhoiBLL();
        hangNPPs = new ArrayList<>();
        chiTietNhapNPP = new ChiTietNhapNPP();
        ctnhs = new ArrayList<>();
        ctnnppbll = new ChiTietNhapNPPBLL();
        nhaPhanPhois = new ArrayList<>();
        nongTrai = new NongTrai();
        nongTraiBLL = new NongTraiBLL();
        nongTrais = new ArrayList<>();
        nhapHangNPPBLL = new NhapHangDLPBLL();
        nhapHangNPP = new NhapHangNPP();

        hangNPPs = nhapHangNPPBLL.getAllNhapHangNPP();

        tbNguyenLieu.setComponentPopupMenu(popupMenuNL);

        try {
            tgn_format = nhapHangNPP.formatDateString(chooseDateNhap.getDate());
        } catch (ParseException ex) {

        }

        txtMaTK.setText(maTK);

        List<NguyenLieu> nguyenLieus = nguyenLieuBLL.getAllNguyenLieu();
        for (NguyenLieu nguyenLieu : nguyenLieus) {
            cbNguyenLieu.addItem(nguyenLieu.getMaNL());
        }

        List<NongTrai> nongTrais = nongTraiBLL.getAllNongTrai();
        for (NongTrai nongTrai : nongTrais) {
            cbMaNT.addItem(nongTrai.getMaNT());
        }

        defaultTableModel = new DefaultTableModel() {
            @Override
            // ham ben duoi duoc xay dung de khong cho user edit du lieu, day nhu la mot anonymous
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        tbNguyenLieu.setModel(defaultTableModel);

        defaultTableModel.addColumn("MÃ NGUYÊN LIỆU");
        defaultTableModel.addColumn("TÊN NGUYÊN LIỆU");
        defaultTableModel.addColumn("MÃ NÔNG TRẠI");
        defaultTableModel.addColumn("MÃ TIÊU CHUẨN");
        defaultTableModel.addColumn("MÃ CHỨNG NHẬN");
    }

    private void loadTableData(int klg, String tgvc) {
        DefaultTableModel model = (DefaultTableModel) tbNguyenLieu.getModel();

        model.addRow(new Object[]{nguyenLieu.getMaNL(), nguyenLieu.getTenNL(), nguyenLieu.getMaNT(), nguyenLieu.getMaTC(), nguyenLieu.getMaCN(), klg, tgvc
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        popupMenuNL = new javax.swing.JPopupMenu();
        itemXoa = new javax.swing.JMenuItem();
        jPanel12 = new javax.swing.JPanel();
        jLabel26 = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbNguyenLieu = new javax.swing.JTable();
        btnThem = new javax.swing.JButton();
        btnTroLai = new javax.swing.JButton();
        jPanel14 = new javax.swing.JPanel();
        jPanel15 = new javax.swing.JPanel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        txtMaPN = new javax.swing.JTextField();
        txtMaTK = new javax.swing.JTextField();
        lbNoti1 = new javax.swing.JLabel();
        lbCheckmaNV = new javax.swing.JLabel();
        chooseDateNhap = new com.toedter.calendar.JDateChooser();
        cbMaNT = new javax.swing.JComboBox<>();
        txtTenNT = new javax.swing.JTextField();
        lbNoti2 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel21 = new javax.swing.JLabel();
        cbNguyenLieu = new javax.swing.JComboBox<>();
        btnChonmua = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        lbNoti3 = new javax.swing.JLabel();
        txtTenNL = new javax.swing.JTextField();
        lbNoti4 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        itemXoa.setText("Xóa");
        itemXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                itemXoaActionPerformed(evt);
            }
        });
        popupMenuNL.add(itemXoa);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel12.setBackground(new java.awt.Color(255, 255, 255));

        jLabel26.setFont(new java.awt.Font("Tahoma", 1, 30)); // NOI18N
        jLabel26.setText("THÊM NHẬP HÀNG NHÀ PHÂN PHỐI");

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel26)
                .addGap(276, 276, 276))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel26)
                .addContainerGap())
        );

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));
        jPanel13.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Danh sách nguyên liệu", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbNguyenLieu.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "", "", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbNguyenLieu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbNguyenLieuMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(tbNguyenLieu);

        btnThem.setText("Thêm");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        btnTroLai.setBackground(new java.awt.Color(204, 255, 153));
        btnTroLai.setText("Trở lại");
        btnTroLai.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTroLaiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addGap(413, 413, 413)
                .addComponent(btnThem)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnTroLai)))
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnThem)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnTroLai)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));
        jPanel15.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin chung", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel29.setText("Mã phiếu nhập NPP: ");

        jLabel30.setText("Mã NPP:");

        jLabel31.setText("Mã nông trại:");

        jLabel32.setText("Thời gian nhập:");

        txtMaPN.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMaPNActionPerformed(evt);
            }
        });

        txtMaTK.setEditable(false);
        txtMaTK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtMaTKActionPerformed(evt);
            }
        });

        lbNoti1.setForeground(new java.awt.Color(255, 51, 0));

        lbCheckmaNV.setForeground(new java.awt.Color(255, 51, 0));

        cbMaNT.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Chọn Mã nông trại" }));
        cbMaNT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMaNTActionPerformed(evt);
            }
        });

        txtTenNT.setEditable(false);

        lbNoti2.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti2.setText("  ");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel31)
                    .addComponent(jLabel29)
                    .addGroup(jPanel15Layout.createSequentialGroup()
                        .addGap(78, 78, 78)
                        .addComponent(lbCheckmaNV, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel32)
                    .addComponent(jLabel30))
                .addGap(18, 18, 18)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lbNoti2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTenNT)
                    .addComponent(lbNoti1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtMaPN)
                    .addComponent(txtMaTK)
                    .addComponent(cbMaNT, 0, 200, Short.MAX_VALUE)
                    .addComponent(chooseDateNhap, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(95, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel29)
                    .addComponent(txtMaPN, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addComponent(lbNoti1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel31)
                    .addComponent(cbMaNT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtTenNT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNoti2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(chooseDateNhap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel32))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel30)
                    .addComponent(txtMaTK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(lbCheckmaNV, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Chi tiết phiếu nhập", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel21.setText("Mã nguyên liệu: ");

        cbNguyenLieu.setMaximumRowCount(50);
        cbNguyenLieu.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Chọn nguyên liệu" }));
        cbNguyenLieu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbNguyenLieuActionPerformed(evt);
            }
        });

        btnChonmua.setBackground(new java.awt.Color(204, 204, 204));
        btnChonmua.setText("Chọn mua");
        btnChonmua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnChonmuaActionPerformed(evt);
            }
        });

        jLabel2.setText("Tên nguyên liệu:");

        lbNoti3.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti3.setText("  ");

        txtTenNL.setEditable(false);

        lbNoti4.setForeground(new java.awt.Color(255, 0, 0));
        lbNoti4.setText("   ");

        jLabel4.setForeground(new java.awt.Color(255, 0, 0));
        jLabel4.setText("  ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(90, 90, 90)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel21)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtTenNL, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lbNoti3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbNguyenLieu, javax.swing.GroupLayout.Alignment.LEADING, 0, 200, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lbNoti4, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnChonmua)
                .addGap(183, 183, 183))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel21)
                    .addComponent(cbNguyenLieu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addComponent(lbNoti3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtTenNL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(8, 8, 8)
                .addComponent(lbNoti4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnChonmua)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap(98, Short.MAX_VALUE)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(87, 87, 87))
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tbNguyenLieuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbNguyenLieuMouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_tbNguyenLieuMouseClicked

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        boolean check = true;
        String maPN = txtMaPN.getText().trim();
        String maNT = String.valueOf(cbMaNT.getSelectedItem());

        if (maPN.equals("")) {
            lbNoti1.setText("Vui lòng nhập mã phiếu nhập!");
        }

        if (cbNguyenLieu.getSelectedIndex() == 0) {
            lbNoti3.setText("Vui lòng chọn mã nguyên liệu!");
            txtTenNL.setText(" ");
        }

        if (cbMaNT.getSelectedIndex() == 0) {
            lbNoti2.setText("Chọn mã nông trại!");
            txtTenNT.setText(" ");
        }

        if ((tgvc_format.compareTo(tgn_format) >= 0)) {
            System.out.println("check time = " + tgvc_format.compareTo(tgn_format));
            lbNoti4.setText("TG vận chuyển phải trước TG nhập!");
        }

        // Chưa bắt lỗi trùng mã món ăn
        if ((!maPN.equals("")) && (cbMaNT.getSelectedIndex() != 0)) {
            for (int i = 0; i < hangNPPs.size(); i++) {
                if (hangNPPs.get(i).getMaPNPP().equals(txtMaPN.getText())) {
                    lbNoti1.setText("Trùng mã phiếu nhập!");
                    check = false;
                    break;
                }
            }

            if (check == true) {
                nhapHangNPP.setMaPNPP(maPN);
                nhapHangNPP.setMaNT(maNT);
                nhapHangNPP.setTgNhap(tgn_format);

                // Phải đúng mã nhà hàng, do là khóa ngoại
                nhapHangNPP.setMaNPP(maTaiKhoan);

                nhapHangNPPBLL.insertNhapHangNPP(nhapHangNPP);

                for (ChiTietNhapNPP ctnh : ctnhs) {
                    ctnnppbll.insertCTPNNPP(ctnh);
                }

                JOptionPane.showMessageDialog(ThemNHNPPView.this, "Thêm phiếu nhập thành công!", "Notification", JOptionPane.INFORMATION_MESSAGE);

                lbNoti1.setText(" ");
                lbNoti2.setText(" ");
                lbNoti3.setText(" ");
                lbNoti4.setText(" ");
                ctnhs = new ArrayList<>();
                txtMaPN.setText("");
                defaultTableModel.setRowCount(0);
            }

        } else {
            JOptionPane.showMessageDialog(ThemNHNPPView.this, "Thêm phiểu nhập thất bại!", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnThemActionPerformed

    private void btnTroLaiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTroLaiActionPerformed
        // TODO add your handling code here:
        new NhapHangNPPView(maTaiKhoan).setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnTroLaiActionPerformed

    private void btnChonmuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnChonmuaActionPerformed
        // TODO add your handling code here:
        boolean check = true;
        tgvc_format = null;

        if (cbNguyenLieu.getSelectedIndex() != 0) {

            Date tg_vc = new Date();
            chiTietNhapNPP = new ChiTietNhapNPP();

            // lay tg van chuyen
            try {
                tgvc_format = chiTietNhapNPP.formatDateString(tg_vc);
                chiTietNhapNPP.setTgVanChuyen(tgvc_format);

            } catch (ParseException ex) {

            }
            // bat loi thoi gian
            if ((tgvc_format.compareTo(tgn_format) >= 0)) {
                lbNoti4.setText("TG vận chuyển phải trước TG nhập!");
            }

            chiTietNhapNPP.setMaPNPP(txtMaPN.getText());
            int sl = 0;
            chiTietNhapNPP.setSoLuong(sl);
            chiTietNhapNPP.setMaNL(String.valueOf(cbNguyenLieu.getSelectedItem()));

            for (int i = 0; i < ctnhs.size(); i++) {
                if (ctnhs.get(i).getMaNL().equals(chiTietNhapNPP.getMaNL())) {
                    lbNoti3.setText("Nguyên liệu đã tồn tại!");
                    check = false;
                    break;
                }
            }

            if (check == true) {
                loadTableData(sl, tgvc_format);
                ctnhs.add(chiTietNhapNPP);
                lbNoti4.setText("  ");
            }
        } else {
            lbNoti3.setText("Vui lòng chọn mã nguyên liệu!");
            txtTenNL.setText(" ");
        }
    }//GEN-LAST:event_btnChonmuaActionPerformed

    private void cbNguyenLieuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbNguyenLieuActionPerformed
        // TODO add your handling code here:
        if (cbNguyenLieu.getSelectedIndex() == 0) {
            lbNoti3.setText("Vui lòng chọn mã nguyên liệu!");
            txtTenNL.setText(" ");
        } else {
            nguyenLieu = nguyenLieuBLL.getNguyenLieuByMaNL(String.valueOf(cbNguyenLieu.getSelectedItem()));
            txtTenNL.setText(nguyenLieu.getTenNL());
            lbNoti3.setText(" ");
        }
    }//GEN-LAST:event_cbNguyenLieuActionPerformed

    private void txtMaTKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMaTKActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaTKActionPerformed

    private void txtMaPNActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtMaPNActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtMaPNActionPerformed

    private void cbMaNTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbMaNTActionPerformed
        // TODO add your handling code here:
        if (cbMaNT.getSelectedIndex() == 0) {
            lbNoti2.setText("Vui lòng chọn mã nông trại!");
            txtTenNT.setText(" ");
        } else {
            nongTrai = nongTraiBLL.getNongTraiByMaNT(String.valueOf(cbMaNT.getSelectedItem()));
            txtTenNT.setText(nongTrai.getTenNT());
            lbNoti2.setText(" ");
        }
    }//GEN-LAST:event_cbMaNTActionPerformed

    private void itemXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_itemXoaActionPerformed
        // TODO add your handling code here:
        int i = tbNguyenLieu.getSelectedRow();

        defaultTableModel.removeRow(i);
        ctnhs.remove(ctnhs.get(i));
    }//GEN-LAST:event_itemXoaActionPerformed

    public static void main(String args[]) {
        // new ThemNHNPPView().setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnChonmua;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnTroLai;
    private javax.swing.JComboBox<String> cbMaNT;
    private javax.swing.JComboBox<String> cbNguyenLieu;
    private com.toedter.calendar.JDateChooser chooseDateNhap;
    private javax.swing.JMenuItem itemXoa;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JLabel lbCheckmaNV;
    private javax.swing.JLabel lbNoti1;
    private javax.swing.JLabel lbNoti2;
    private javax.swing.JLabel lbNoti3;
    private javax.swing.JLabel lbNoti4;
    private javax.swing.JPopupMenu popupMenuNL;
    private javax.swing.JTable tbNguyenLieu;
    private javax.swing.JTextField txtMaPN;
    private javax.swing.JTextField txtMaTK;
    private javax.swing.JTextField txtTenNL;
    private javax.swing.JTextField txtTenNT;
    // End of variables declaration//GEN-END:variables
}
