package DAO;

import UTILS.JDBCConnection;
import DTO.ChungNhan;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ChungNhanDAO {
    public List<ChungNhan> getAllChungNhan() {
        List<ChungNhan> chungNhans = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM CHUNGNHAN";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                ChungNhan chungNhan = new ChungNhan();
                
                chungNhan.setMaCN(rs.getString("MA_CN"));
                chungNhan.setTenCN(rs.getString("TEN_CN"));
                chungNhan.setKiemTra(rs.getString("KIEM_TRA"));     
                
                chungNhans.add(chungNhan);
            }
        } catch (SQLException e) {
        }
        
        return chungNhans;
    }
    
    public ChungNhan getChungNhanByMaCN(String maCN) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM CHUNGNHAN WHERE MA_CN = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maCN);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                ChungNhan chungNhan = new ChungNhan();
                
                chungNhan.setMaCN(rs.getString("MA_CN"));
                chungNhan.setTenCN(rs.getString("TEN_CN"));
                chungNhan.setKiemTra(rs.getString("KIEM_TRA"));       
                
                return chungNhan;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
}
