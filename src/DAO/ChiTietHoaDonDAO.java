package DAO;

import UTILS.JDBCConnection;
import DTO.ChiTietHoaDon;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ChiTietHoaDonDAO {
    public List<ChiTietHoaDon> getAllChiTietHoaDon() {
        List<ChiTietHoaDon> cthds = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM CHITIETHOADON";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                ChiTietHoaDon ctHoaDon = new ChiTietHoaDon();
                
                ctHoaDon.setMaHD(rs.getString("MA_HD"));
                ctHoaDon.setMaMonAn(rs.getString("MA_MON_AN"));
                ctHoaDon.setSoLuong(rs.getInt("SO_LUONG"));
                
                cthds.add(ctHoaDon);
            }
        } catch (SQLException e) {
        }
        
        return cthds;
    }
    
    public List<ChiTietHoaDon> getCTHDByMaHD(String maHD) {
        
        List<ChiTietHoaDon> cthds = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM CHITIETHOADON WHERE MA_HD = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maHD);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                ChiTietHoaDon ctHoaDon = new ChiTietHoaDon();
                
                ctHoaDon.setMaHD(rs.getString("MA_HD"));
                ctHoaDon.setMaMonAn(rs.getString("MA_MON_AN"));
                ctHoaDon.setSoLuong(rs.getInt("SO_LUONG"));
                
                cthds.add(ctHoaDon);
                                
            }
        } catch (SQLException e) {
        }
        
        return cthds;
    }
    
    public void insertCTHD(ChiTietHoaDon cthd){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO CHITIETHOADON (MA_HD, MA_MON_AN, SO_LUONG) VALUES (?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, cthd.getMaHD());
            preparedStatement.setString(2, cthd.getMaMonAn());
            preparedStatement.setInt(3, cthd.getSoLuong());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
}
