package DAO;

import UTILS.JDBCConnection;
import DTO.ChiTietNhapDaiLy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ChiTietNhapDaiLyDAO {
    public List<ChiTietNhapDaiLy> getAllChiTietNhapNH() {
        List<ChiTietNhapDaiLy> ctnnhs = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM CHITIETNHAP_NH";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                ChiTietNhapDaiLy ctnnh = new ChiTietNhapDaiLy();
                
                ctnnh.setMaPNNH(rs.getString("MA_PNNH"));
                ctnnh.setMaNL(rs.getString("MA_NL"));
                ctnnh.setSoLuong(rs.getInt("SO_LUONG"));    
                ctnnh.setTgVanChuyen(rs.getString("THOI_GIAN_VAN_CHUYEN"));
                
                ctnnhs.add(ctnnh);
            }
        } catch (SQLException e) {
        }
        
        return ctnnhs;
    }
    
    public List<ChiTietNhapDaiLy> getCTPNByMaPNNH(String maPNNH) {
        List<ChiTietNhapDaiLy> ctnnhs = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM CHITIETNHAP_NH WHERE MA_PNNH = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maPNNH);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                ChiTietNhapDaiLy ctnnh = new ChiTietNhapDaiLy();
                
                ctnnh.setMaPNNH(rs.getString("MA_PNNH"));
                ctnnh.setMaNL(rs.getString("MA_NL"));
                ctnnh.setSoLuong(rs.getInt("SO_LUONG"));    
                ctnnh.setTgVanChuyen(rs.getString("THOI_GIAN_VAN_CHUYEN"));
                
                ctnnhs.add(ctnnh);
            }
        } catch (SQLException e) {
        }
        
        return ctnnhs;
    }
    
    public void insertCTPN(ChiTietNhapDaiLy ctnnh){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO CHITIETNHAP_NH (MA_PNNH, MA_NL, SO_LUONG, THOI_GIAN_VAN_CHUYEN) VALUES (?,?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, ctnnh.getMaPNNH());
            preparedStatement.setString(2, ctnnh.getMaNL());
            preparedStatement.setInt(3, ctnnh.getSoLuong());
            preparedStatement.setString(4, ctnnh.getTgVanChuyen());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
}
