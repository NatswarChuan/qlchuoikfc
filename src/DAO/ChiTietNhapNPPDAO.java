package DAO;

import UTILS.JDBCConnection;
import DTO.ChiTietNhapNPP;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ChiTietNhapNPPDAO {
    public List<ChiTietNhapNPP> getAllChiTietNhapNPP() {
        List<ChiTietNhapNPP> ctnnpps = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM CHITIETNHAP_NPP";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                ChiTietNhapNPP ctnnpp = new ChiTietNhapNPP();
                
                ctnnpp.setMaPNPP(rs.getString("MA_PNPP"));
                ctnnpp.setMaNL(rs.getString("MA_NL"));
                ctnnpp.setSoLuong(rs.getInt("SO_LUONG"));    
                ctnnpp.setTgVanChuyen(rs.getString("THOI_GIAN_VAN_CHUYEN"));
                
                ctnnpps.add(ctnnpp);
            }
        } catch (SQLException e) {
        }
        
        return ctnnpps;
    }
    
    public List<ChiTietNhapNPP> getCTPNByMaPNPP(String maPNPP) {
        List<ChiTietNhapNPP> ctnnpps = new ArrayList<>();
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM CHITIETNHAP_NPP WHERE MA_PNPP = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maPNPP);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                ChiTietNhapNPP ctnnpp = new ChiTietNhapNPP();
                
                ctnnpp.setMaPNPP(rs.getString("MA_PNPP"));
                ctnnpp.setMaNL(rs.getString("MA_NL"));
                ctnnpp.setSoLuong(rs.getInt("SO_LUONG"));    
                ctnnpp.setTgVanChuyen(rs.getString("THOI_GIAN_VAN_CHUYEN"));               
                
                ctnnpps.add(ctnnpp);
                
            }
        } catch (SQLException e) {
        }
        
        return ctnnpps;
    }
    
    public void insertCTPNNPP(ChiTietNhapNPP ctnnpp){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO CHITIETNHAP_NPP (MA_PNPP, MA_NL, SO_LUONG, THOI_GIAN_VAN_CHUYEN) VALUES (?,?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, ctnnpp.getMaPNPP());
            preparedStatement.setString(2, ctnnpp.getMaNL());
            preparedStatement.setInt(3, ctnnpp.getSoLuong());
            preparedStatement.setString(4, ctnnpp.getTgVanChuyen());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
}
