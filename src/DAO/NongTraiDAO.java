package DAO;

import UTILS.JDBCConnection;
import DTO.NongTrai;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NongTraiDAO {

    public List<NongTrai> getAllNongTrai() {
        List<NongTrai> nongTrais = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NONGTRAI";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NongTrai nongTrai = new NongTrai();
                
                nongTrai.setMaNT(rs.getString("MA_NT"));
                nongTrai.setTenNT(rs.getString("TEN_NT"));
                nongTrai.setChuSoHuu(rs.getString("CHU_SO_HUU"));
                nongTrai.setDiaChi(rs.getString("DIA_CHI"));
                nongTrai.setSDT(rs.getString("SDT"));
                nongTrai.setMaTK(rs.getString("MA_TK"));
                
                nongTrais.add(nongTrai);
            }
        } catch (SQLException e) {
        }
        
        return nongTrais;
    }
    
    public NongTrai getNongTraiByMaNT(String maNT) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NONGTRAI WHERE MA_NT = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maNT);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NongTrai nongTrai = new NongTrai();
                
                nongTrai.setMaNT(rs.getString("MA_NT"));
                nongTrai.setTenNT(rs.getString("TEN_NT"));
                nongTrai.setChuSoHuu(rs.getString("CHU_SO_HUU"));
                nongTrai.setDiaChi(rs.getString("DIA_CHI"));
                nongTrai.setSDT(rs.getString("SDT"));
                nongTrai.setMaTK(rs.getString("MA_TK"));
                
                return nongTrai;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    public void insertNongTrai(NongTrai nongTrai){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO NONGTRAI (MA_NT, TEN_NT, CHU_SO_HUU, DIA_CHI, SDT, MA_TK) VALUES (?,?,?,?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nongTrai.getMaNT());
            preparedStatement.setString(2, nongTrai.getTenNT());
            preparedStatement.setString(3, nongTrai.getChuSoHuu());
            preparedStatement.setString(4, nongTrai.getDiaChi());
            preparedStatement.setString(5, nongTrai.getSDT());
            preparedStatement.setString(6, nongTrai.getMaTK());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
}
