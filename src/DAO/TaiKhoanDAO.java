package DAO;

import UTILS.JDBCConnection;
import DTO.TaiKhoan;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TaiKhoanDAO {
    public List<TaiKhoan> getAllTaiKhoan() {
        List<TaiKhoan> taiKhoans = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM TAIKHOAN";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                TaiKhoan taiKhoan = new TaiKhoan();
                
                taiKhoan.setMaTK(rs.getString("MA_TK"));
                taiKhoan.setPassword(rs.getString("PASSWORD"));
                taiKhoan.setVaitro(rs.getString("VAI_TRO"));
                
                taiKhoans.add(taiKhoan);
            }
        } catch (SQLException e) {
        }
        
        return taiKhoans;
    }
    
    public TaiKhoan getTaiKhoanByMaTK(String maTK) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM TAIKHOAN WHERE MA_TK = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maTK);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                TaiKhoan taiKhoan = new TaiKhoan();
                
                taiKhoan.setMaTK(rs.getString("MA_TK"));
                taiKhoan.setPassword(rs.getString("PASSWORD"));
                taiKhoan.setVaitro(rs.getString("VAI_TRO"));
                
                return taiKhoan;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    public void insertTaiKhoan(TaiKhoan taiKhoan){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO TAIKHOAN (MA_TK, PASSWORD, VAI_TRO) VALUES (?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, taiKhoan.getMaTK());
            preparedStatement.setString(2, taiKhoan.getPassword());
            preparedStatement.setString(3, taiKhoan.getVaitro());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
    
    public void deleteTaiKhoan(String maTK){
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "DELETE FROM TAIKHOAN WHERE MA_TK = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maTK);
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
    }
    
    public void updateTaikhoan(TaiKhoan taikhoan){
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "UPDATE TAIKHOAN SET PASSWORD = ?, VAI_TRO = ? WHERE MA_TK = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);       
            preparedStatement.setString(1, taikhoan.getPassword()); 
            preparedStatement.setString(2, taikhoan.getVaitro());  
            preparedStatement.setString(3, taikhoan.getMaTK());        

            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }    
    } 
}
