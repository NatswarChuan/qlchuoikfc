package DAO;

import UTILS.JDBCConnection;
import DTO.NguonGoc;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NguonGocDAO {
    public List<NguonGoc> getAllNguonGoc() {
        List<NguonGoc> nguonGocs = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NGUONGOC";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NguonGoc nguonGoc = new NguonGoc();
                
                nguonGoc.setMaMonAn(rs.getString("MA_MON_AN"));
                nguonGoc.setMaNL(rs.getString("MA_NL"));
                
                nguonGocs.add(nguonGoc);
            }
        } catch (SQLException e) {
        }
        
        return nguonGocs;
    }
    
    public List<NguonGoc> getNguonGocbyMaMA(String maMonAn) {
        List<NguonGoc> nguonGocs = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NGUONGOC WHERE MA_MON_AN = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maMonAn);
            ResultSet rs = preparedStatement.executeQuery();
  
            while(rs.next()){
                NguonGoc nguonGoc = new NguonGoc();
                
                nguonGoc.setMaMonAn(rs.getString("MA_MON_AN"));
                nguonGoc.setMaNL(rs.getString("MA_NL"));
                
                nguonGocs.add(nguonGoc);
            }
        } catch (SQLException e) {
        }
        
        return nguonGocs;
    }
    
    public void insertNguonGoc(NguonGoc nguonGoc){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO NGUONGOC (MA_MON_AN, MA_NL) VALUES (?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            preparedStatement.setString(1, nguonGoc.getMaMonAn());
            preparedStatement.setString(2, nguonGoc.getMaNL());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
    
    public void deleteNguonGocByMaNL(String maNL){
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "DELETE FROM NGUONGOC WHERE MA_NL = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maNL);
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
    }
    
    public void deleteNguonGocByMaMA(String maMa){
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "DELETE FROM NGUONGOC WHERE MA_MON_AN = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maMa);
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
    }
    
    public void deleteNguonGoc(String maMa, String maNL){
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "DELETE FROM NGUONGOC WHERE MA_MON_AN = ? AND MA_NL = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maMa);
            preparedStatement.setString(2, maNL);
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
    }
    
//    public void updateNguonGoc(NguonGoc nguonGoc){
//        Connection connection = JDBCConnection.getConnection();
//        
//        String sql = "UPDATE NGUONGOC SET MA_NL = ? WHERE MA_MON_AN = ?";
//        
//        try {
//            PreparedStatement preparedStatement = connection.prepareStatement(sql);       
//            
//            preparedStatement.setString(1, nguonGoc.getMaNL());
//            preparedStatement.setString(2, nguonGoc.getMaMonAn());
//
//            int rs = preparedStatement.executeUpdate();
//            System.out.println(rs);
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }    
//    }
}
