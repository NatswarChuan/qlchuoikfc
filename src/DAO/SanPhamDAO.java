package DAO;

import UTILS.JDBCConnection;
import DTO.SanPham;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SanPhamDAO {
    public List<SanPham> getAllMonAn() {
        List<SanPham> monAns = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM MONAN";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                SanPham monAn = new SanPham();
                
                monAn.setMaMonAn(rs.getString("MA_MON_AN"));
                monAn.setTenMonAn(rs.getString("TEN_MON_AN"));
                monAn.setTgCheBien(rs.getString("TG_CHE_BIEN"));              
                monAn.setMaNH(rs.getString("MA_NH"));
                monAn.setMaCN(rs.getString("MA_CN"));
                monAn.setKiemNghiem(rs.getString("KIEM_NGHIEM"));
                
                monAns.add(monAn);
            }
        } catch (SQLException e) {
        }
        
        return monAns;
    }
    
    public SanPham getMonAnByMaMA(String maMA) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM MONAN WHERE MA_MON_AN = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maMA);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                SanPham monAn = new SanPham();
                
                monAn.setMaMonAn(rs.getString("MA_MON_AN"));
                monAn.setTenMonAn(rs.getString("TEN_MON_AN"));
                monAn.setTgCheBien(rs.getString("TG_CHE_BIEN"));              
                monAn.setMaNH(rs.getString("MA_NH"));
                monAn.setMaCN(rs.getString("MA_CN"));
                monAn.setKiemNghiem(rs.getString("KIEM_NGHIEM"));
                
                return monAn;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    public void insertMonAn(SanPham monAn){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO MONAN (MA_MON_AN, TEN_MON_AN, TG_CHE_BIEN, MA_NH, MA_CN, KIEM_NGHIEM) VALUES (?,?,?,?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            preparedStatement.setString(1, monAn.getMaMonAn());
            preparedStatement.setString(2, monAn.getTenMonAn());
            preparedStatement.setString(3, monAn.getTgCheBien());
            preparedStatement.setString(4, monAn.getMaNH());
            preparedStatement.setString(5, monAn.getMaCN());
            preparedStatement.setString(6, monAn.getKiemNghiem());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
    
    public void deleteMonAn(String maMA){
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "DELETE FROM MONAN WHERE MA_MON_AN = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maMA);
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
    }
    
    public void updateMonAn(SanPham monAn){
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "UPDATE MONAN SET TEN_MON_AN = ?, TG_CHE_BIEN = ?, MA_NH = ?, MA_CN = ?, KIEM_NGHIEM = ? WHERE MA_MON_AN = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);       
            
            preparedStatement.setString(1, monAn.getTenMonAn());
            preparedStatement.setString(2, monAn.getTgCheBien());
            preparedStatement.setString(3, monAn.getMaNH());
            preparedStatement.setString(4, monAn.getMaCN());
            preparedStatement.setString(5, monAn.getKiemNghiem());
            preparedStatement.setString(6, monAn.getMaMonAn());

            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }    
    }
}
