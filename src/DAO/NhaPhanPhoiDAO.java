package DAO;

import UTILS.JDBCConnection;
import DTO.NhaPhanPhoi;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NhaPhanPhoiDAO {
    public List<NhaPhanPhoi> getAllNhaPhanPhoi() {
        List<NhaPhanPhoi> nhaPhanPhois = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NHAPHANPHOI";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NhaPhanPhoi nhaPhanPhoi = new NhaPhanPhoi();
                
                nhaPhanPhoi.setMaNPP(rs.getString("MA_NPP"));
                nhaPhanPhoi.setTenNPP(rs.getString("TEN_NPP"));
                nhaPhanPhoi.setDiaChi(rs.getString("DIA_CHI"));
                nhaPhanPhoi.setSDT(rs.getString("SDT"));
                nhaPhanPhoi.setMaTK(rs.getString("MA_TK"));
                
                nhaPhanPhois.add(nhaPhanPhoi);
            }
        } catch (SQLException e) {
        }
        
        return nhaPhanPhois;
    }
    
    public NhaPhanPhoi getNPPByMaNPP(String maNPP) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NHAPHANPHOI WHERE MA_NPP = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maNPP);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NhaPhanPhoi nhaPhanPhoi = new NhaPhanPhoi();
                
                nhaPhanPhoi.setMaNPP(rs.getString("MA_NPP"));
                nhaPhanPhoi.setTenNPP(rs.getString("TEN_NPP"));
                nhaPhanPhoi.setDiaChi(rs.getString("DIA_CHI"));
                nhaPhanPhoi.setSDT(rs.getString("SDT"));
                nhaPhanPhoi.setMaTK(rs.getString("MA_TK"));
                
                return nhaPhanPhoi;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    public void insertNhaPhanPhoi(NhaPhanPhoi nhaPhanPhoi){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO NHAPHANPHOI (MA_NPP, TEN_NPP, DIA_CHI, SDT, MA_TK) VALUES (?,?,?,?,?)";
        System.out.println(nhaPhanPhoi.getDiaChi());
        System.out.println(nhaPhanPhoi.getMaNPP());
        System.out.println(nhaPhanPhoi.getMaTK());
        System.out.println(nhaPhanPhoi.getSDT());
        System.out.println(nhaPhanPhoi.getTenNPP());
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nhaPhanPhoi.getMaNPP());
            preparedStatement.setString(2, nhaPhanPhoi.getTenNPP());
            preparedStatement.setString(3, nhaPhanPhoi.getDiaChi());
            preparedStatement.setString(4, nhaPhanPhoi.getSDT());
            preparedStatement.setString(5, nhaPhanPhoi.getMaTK());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
            System.out.println(e);
        }
        
    }
}
