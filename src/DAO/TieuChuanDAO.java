package DAO;

import UTILS.JDBCConnection;
import DTO.TieuChuan;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TieuChuanDAO {
    public List<TieuChuan> getAllChungNhan() {
        List<TieuChuan> tieuChuans = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM TIEUCHUAN";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                TieuChuan tieuChuan = new TieuChuan();
                
                tieuChuan.setMaTC(rs.getString("MA_TC"));
                tieuChuan.setTenTC(rs.getString("TEN_TC"));
                tieuChuan.setKiemTra(rs.getString("KIEM_TRA"));     
                
                tieuChuans.add(tieuChuan);
            }
        } catch (SQLException e) {
        }
        
        return tieuChuans;
    }
    
    public TieuChuan getTieuChuanByMaTC(String maTC) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM TIEUCHUAN WHERE MA_TC = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maTC);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                TieuChuan tieuChuan = new TieuChuan();
                
                tieuChuan.setMaTC(rs.getString("MA_TC"));
                tieuChuan.setTenTC(rs.getString("TEN_TC"));
                tieuChuan.setKiemTra(rs.getString("KIEM_TRA"));      
                
                return tieuChuan;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
}
