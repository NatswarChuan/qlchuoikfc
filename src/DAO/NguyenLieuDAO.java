package DAO;

import UTILS.JDBCConnection;
import DTO.NguyenLieu;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NguyenLieuDAO {
    public List<NguyenLieu> getAllNguyenLieu() {
        List<NguyenLieu> nguyenLieus = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NGUYENLIEU";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NguyenLieu nguyenLieu = new NguyenLieu();
                
                nguyenLieu.setMaNL(rs.getString("MA_NL"));
                nguyenLieu.setTenNL(rs.getString("TEN_NL"));
                nguyenLieu.setMaNT(rs.getString("MA_NT"));
                nguyenLieu.setMaTC(rs.getString("MA_TC"));              
                nguyenLieu.setMaCN(rs.getString("MA_CN"));
                
                nguyenLieus.add(nguyenLieu);
            }
        } catch (SQLException e) {
        }
        
        return nguyenLieus;
    }
    
    public NguyenLieu getNguyenLieuByMaNL(String maNL) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NGUYENLIEU WHERE MA_NL = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maNL);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NguyenLieu nguyenLieu = new NguyenLieu();
                
                nguyenLieu.setMaNL(rs.getString("MA_NL"));
                nguyenLieu.setTenNL(rs.getString("TEN_NL"));
                nguyenLieu.setMaNT(rs.getString("MA_NT"));
                nguyenLieu.setMaTC(rs.getString("MA_TC"));              
                nguyenLieu.setMaCN(rs.getString("MA_CN"));
                
                return nguyenLieu;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    public void insertNguyenLieu(NguyenLieu nguyenLieu){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO NGUYENLIEU (MA_NL, TEN_NL, MA_NT, MA_TC, MA_CN) VALUES (?,?,?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nguyenLieu.getMaNL());
            preparedStatement.setString(2, nguyenLieu.getTenNL());
            preparedStatement.setString(3, nguyenLieu.getMaNT());
            preparedStatement.setString(4, nguyenLieu.getMaTC());
            preparedStatement.setString(5, nguyenLieu.getMaCN());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
    
    public void deleteNguyenLieu(String maNL){
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "DELETE FROM NGUYENLIEU WHERE MA_NL = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maNL);
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
    }
    
    public void updateNguyenLieu(NguyenLieu nguyenLieu){
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "UPDATE NGUYENLIEU SET TEN_NL = ?, MA_NT = ?, MA_TC = ?, MA_CN = ? WHERE MA_NL = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);       
            
            preparedStatement.setString(1, nguyenLieu.getTenNL());
            preparedStatement.setString(2, nguyenLieu.getMaNT());
            preparedStatement.setString(3, nguyenLieu.getMaTC());
            preparedStatement.setString(4, nguyenLieu.getMaCN()); 
            preparedStatement.setString(5, nguyenLieu.getMaNL());

            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
            e.printStackTrace();
        }    
    }
}
