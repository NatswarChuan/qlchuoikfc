package DAO;

import UTILS.JDBCConnection;
import DTO.NhapHangDaiLY;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NhapHangDaiLyDAO {
    public List<NhapHangDaiLY> getAllNhapHangNHs() {
        List<NhapHangDaiLY> nhapHangNHs = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NHAPHANG_NH";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NhapHangDaiLY nhapHangNH = new NhapHangDaiLY();
                
                nhapHangNH.setMaPNNH(rs.getString("MA_PNNH"));
                nhapHangNH.setMaNPP(rs.getString("MA_NPP"));
                nhapHangNH.setTgNhap(rs.getString("THOI_GIAN_NHAP"));
                nhapHangNH.setMaNH(rs.getString("MA_NH"));           
                
                nhapHangNHs.add(nhapHangNH);
            }
        } catch (SQLException e) {
        }
        
        return nhapHangNHs;
    }
    
    public NhapHangDaiLY getNHNHByMaNH(String maPNNH) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NHAPHANG_NH WHERE MA_PNNH = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maPNNH);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NhapHangDaiLY nhapHangNH = new NhapHangDaiLY();
                
                nhapHangNH.setMaPNNH(rs.getString("MA_PNNH"));
                nhapHangNH.setMaNPP(rs.getString("MA_NPP"));
                nhapHangNH.setTgNhap(rs.getString("THOI_GIAN_NHAP"));
                nhapHangNH.setMaNH(rs.getString("MA_NH")); 
                
                return nhapHangNH;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    public void insertNhapHangNH(NhapHangDaiLY nhapHangNH){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO NHAPHANG_NH (MA_PNNH, MA_NPP, THOI_GIAN_NHAP, MA_NH) VALUES (?,?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nhapHangNH.getMaPNNH());
            preparedStatement.setString(2, nhapHangNH.getMaNPP());
            preparedStatement.setString(3, nhapHangNH.getTgNhap());
            preparedStatement.setString(4, nhapHangNH.getMaNH());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
}