package DAO;

import UTILS.JDBCConnection;
import DTO.HoaDon;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class HoaDonDAO {
    public List<HoaDon> getAllHoaDon() {
        List<HoaDon> hoaDons = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM HOADON";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                HoaDon hoaDon = new HoaDon();
                
                hoaDon.setMaHD(rs.getString("MA_HD"));
                hoaDon.setNgayXuat(rs.getString("NGAY_XUAT"));
                hoaDon.setTenKH(rs.getString("TEN_KH"));
                hoaDon.setMaNH(rs.getString("MA_NH"));           
                
                hoaDons.add(hoaDon);
            }
        } catch (SQLException e) {
        }
        
        return hoaDons;
    }
    
    public HoaDon getHoaDonByMaHD(String maNL) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM HOADON WHERE MA_HD = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maNL);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                HoaDon hoaDon = new HoaDon();
                
                hoaDon.setMaHD(rs.getString("MA_HD"));
                hoaDon.setNgayXuat(rs.getString("NGAY_XUAT"));
                hoaDon.setTenKH(rs.getString("TEN_KH"));
                hoaDon.setMaNH(rs.getString("MA_NH"));
                
                return hoaDon;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    public void insertHoaDon(HoaDon hoaDon){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO HOADON (MA_HD, NGAY_XUAT, TEN_KH, MA_NH) VALUES (?,?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, hoaDon.getMaHD());
            preparedStatement.setString(2, hoaDon.getNgayXuat());
            preparedStatement.setString(3, hoaDon.getTenKH());
            preparedStatement.setString(4, hoaDon.getMaNH());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
}
