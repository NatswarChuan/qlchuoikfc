package DAO;

import UTILS.JDBCConnection;
import DTO.NhapHangNPP;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class NhapHangNPPDAO {
    public List<NhapHangNPP> getAllNhapHangNPP() {
        List<NhapHangNPP> nhapHangNPPs = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NHAPHANG_NPP";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NhapHangNPP nhapHangNPP = new NhapHangNPP();
                
                nhapHangNPP.setMaPNPP(rs.getString("MA_PNPP"));
                nhapHangNPP.setMaNT(rs.getString("MA_NT"));
                nhapHangNPP.setTgNhap(rs.getString("THOI_GIAN_NHAP"));
                nhapHangNPP.setMaNPP(rs.getString("MA_NPP"));           
                
                nhapHangNPPs.add(nhapHangNPP);
            }
        } catch (SQLException e) {
        }
        
        return nhapHangNPPs;
    }
    
    public NhapHangNPP getNHNPPByMaNPP(String maNPP) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NHAPHANG_NPP WHERE MA_PNPP = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maNPP);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                NhapHangNPP nhapHangNPP = new NhapHangNPP();
                
                nhapHangNPP.setMaPNPP(rs.getString("MA_PNPP"));
                nhapHangNPP.setMaNT(rs.getString("MA_NT"));
                nhapHangNPP.setTgNhap(rs.getString("THOI_GIAN_NHAP"));
                nhapHangNPP.setMaNPP(rs.getString("MA_NPP"));  
                
                return nhapHangNPP;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    public void insertNhapHangNPP(NhapHangNPP nhapHangNPP){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO NHAPHANG_NPP (MA_PNPP, MA_NT, THOI_GIAN_NHAP, MA_NPP) VALUES (?,?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nhapHangNPP.getMaPNPP());
            preparedStatement.setString(2, nhapHangNPP.getMaNT());
            preparedStatement.setString(3, nhapHangNPP.getTgNhap());
            preparedStatement.setString(4, nhapHangNPP.getMaNPP());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
}
