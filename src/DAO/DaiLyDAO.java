package DAO;

import UTILS.JDBCConnection;
import DTO.DaiLy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DaiLyDAO {
    public List<DaiLy> getAllNhaHang() {
        List<DaiLy> nhaHangs = new ArrayList<>();
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NHAHANG";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                DaiLy nhaHang = new DaiLy();
                
                nhaHang.setMaNH(rs.getString("MA_NH"));
                nhaHang.setTenNH(rs.getString("TEN_NH"));
                nhaHang.setHopDongMB(rs.getString("HOP_DONG_MUA_BAN"));
                nhaHang.setTgNhanHang(rs.getString("TG_NHAP_HANG"));
                nhaHang.setDiaChi(rs.getString("DIA_CHI"));
                nhaHang.setSDT(rs.getString("SDT"));
                nhaHang.setMaTK(rs.getString("MA_TK"));
                
                nhaHangs.add(nhaHang);
            }
        } catch (SQLException e) {
        }
        
        return nhaHangs;
    }
    
    public DaiLy getNhaHangByMaNH(String maNH) {
        
        Connection connection = JDBCConnection.getConnection();
        
        String sql = "SELECT * FROM NHAHANG WHERE MA_NH = ?";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, maNH);
            ResultSet rs = preparedStatement.executeQuery();
            
            while(rs.next()){
                DaiLy nhaHang = new DaiLy();
                
                nhaHang.setMaNH(rs.getString("MA_NH"));
                nhaHang.setTenNH(rs.getString("TEN_NH"));
                nhaHang.setHopDongMB(rs.getString("HOP_DONG_MUA_BAN"));
                nhaHang.setTgNhanHang(rs.getString("TG_NHAP_HANG"));
                nhaHang.setDiaChi(rs.getString("DIA_CHI"));
                nhaHang.setSDT(rs.getString("SDT"));
                nhaHang.setMaTK(rs.getString("MA_TK"));
                
                return nhaHang;
            }
        } catch (SQLException e) {
        }
        
        return null;
    }
    
    public void insertNhaHang(DaiLy nhaHang){
        Connection connection = JDBCConnection.getConnection();
        String sql = "INSERT INTO NHAHANG (MA_NH, TEN_NH, HOP_DONG_MUA_BAN, TG_NHAP_HANG,DIA_CHI, SDT, MA_TK) VALUES (?,?,?,?,?,?,?)";
        
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, nhaHang.getMaNH());
            preparedStatement.setString(2, nhaHang.getTenNH());
            preparedStatement.setString(3, nhaHang.getHopDongMB());
            preparedStatement.setString(4, nhaHang.getTgNhanHang());
            preparedStatement.setString(5, nhaHang.getDiaChi());
            preparedStatement.setString(6, nhaHang.getSDT());
            preparedStatement.setString(7, nhaHang.getMaTK());
            
            int rs = preparedStatement.executeUpdate();
            System.out.println(rs);
        } catch (SQLException e) {
        }
        
    }
}
