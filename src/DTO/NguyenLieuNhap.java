package DTO;

public class NguyenLieuNhap {
    private String maNLN;
    private String tenNLN;

    public NguyenLieuNhap() {
    }

    public NguyenLieuNhap(String maNLN, String tenNLN) {
        this.maNLN = maNLN;
        this.tenNLN = tenNLN;
    }

    public String getMaNLN() {
        return maNLN;
    }

    public void setMaNLN(String maNLN) {
        this.maNLN = maNLN;
    }

    public String getTenNLN() {
        return tenNLN;
    }

    public void setTenNLN(String tenNLN) {
        this.tenNLN = tenNLN;
    }
    
}
