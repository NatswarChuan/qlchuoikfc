package DTO;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NhapHangDaiLY {
    private String maPNNH;
    private String maNPP;
    private String tgNhap;
    private String maNH;

    public NhapHangDaiLY() {
    }

    public NhapHangDaiLY(String maPNNH, String maNPP, String tgNhap, String maNH) {
        this.maPNNH = maPNNH;
        this.maNPP = maNPP;
        this.tgNhap = tgNhap;
        this.maNH = maNH;
    }

    public String getMaPNNH() {
        return maPNNH;
    }

    public void setMaPNNH(String maPNNH) {
        this.maPNNH = maPNNH;
    }

    public String getMaNPP() {
        return maNPP;
    }

    public void setMaNPP(String maNPP) {
        this.maNPP = maNPP;
    }

    public String getTgNhap() {
        return tgNhap;
    }

    public void setTgNhap(String tgNhap) {
        this.tgNhap = tgNhap;
    }

    public String getMaNH() {
        return maNH;
    }

    public void setMaNH(String maNH) {
        this.maNH = maNH;
    }
    
    public String formatDateString (Date date) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        String dateString = df.format(date);
        return dateString;
    }
    
    public Date formatStringDate (String dateString) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = df.parse(dateString);
        return date;
    }
    
}
