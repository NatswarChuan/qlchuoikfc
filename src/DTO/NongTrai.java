package DTO;

public class NongTrai {
    private String maNT;
    private String tenNT;
    private String chuSoHuu;
    private String diaChi;
    private String SDT;
    private String maTK;

    public NongTrai() {
    }

    public NongTrai(String maNT, String tenNT, String chuSoHuu, String diaChi, String SDT, String maTK) {
        this.maNT = maNT;
        this.tenNT = tenNT;
        this.chuSoHuu = chuSoHuu;
        this.diaChi = diaChi;
        this.SDT = SDT;
        this.maTK = maTK;
    }

    public String getMaNT() {
        return maNT;
    }

    public void setMaNT(String maNT) {
        this.maNT = maNT;
    }

    public String getTenNT() {
        return tenNT;
    }

    public void setTenNT(String tenNT) {
        this.tenNT = tenNT;
    }

    public String getChuSoHuu() {
        return chuSoHuu;
    }

    public void setChuSoHuu(String chuSoHuu) {
        this.chuSoHuu = chuSoHuu;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getMaTK() {
        return maTK;
    }

    public void setMaTK(String maTK) {
        this.maTK = maTK;
    }
    
    
}
