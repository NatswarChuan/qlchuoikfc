package DTO;

public class TaiKhoan {
    private String maTK;
    private String password;
    private String vaitro;

    public TaiKhoan() {
    }

    public TaiKhoan(String maTK, String password, String vaitro) {
        this.maTK = maTK;
        this.password = password;
        this.vaitro = vaitro;
    }

    public String getMaTK() {
        return maTK;
    }

    public void setMaTK(String maTK) {
        this.maTK = maTK;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVaitro() {
        return vaitro;
    }

    public void setVaitro(String vaitro) {
        this.vaitro = vaitro;
    } 
}
