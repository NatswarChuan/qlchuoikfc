package DTO;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class HoaDon {
    private String maHD;
    private String ngayXuat;
    private String tenKH;
    private String maNH;

    public HoaDon() {
    }

    public HoaDon(String maHD, String ngayXuat, String tenKH, String maNH) {
        this.maHD = maHD;
        this.ngayXuat = ngayXuat;
        this.tenKH = tenKH;
        this.maNH = maNH;
    }

    public String getMaNH() {
        return maNH;
    }

    public void setMaNH(String maNH) {
        this.maNH = maNH;
    }

    public String getMaHD() {
        return maHD;
    }

    public void setMaHD(String maHD) {
        this.maHD = maHD;
    }

    public String getNgayXuat() {
        return ngayXuat;
    }

    public void setNgayXuat(String ngayXuat) {
        this.ngayXuat = ngayXuat;
    }

    public String getTenKH() {
        return tenKH;
    }

    public void setTenKH(String tenKH) {
        this.tenKH = tenKH;
    }
    
    public String formatDateString (Date date) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        String dateString = df.format(date);
        return dateString;
    }
    
    public Date formatStringDate (String dateString) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = df.parse(dateString);
        return date;
    }
}
