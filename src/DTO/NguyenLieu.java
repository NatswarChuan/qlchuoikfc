package DTO;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NguyenLieu {
    private String maNL;
    private String tenNL;
    private String maNT;
    private String maTC;
    private String maCN;

    public NguyenLieu() {
    }

    public NguyenLieu(String maNL, String tenNL, String maNT, String maTC, String maCN) {
        this.maNL = maNL;
        this.tenNL = tenNL;
        this.maNT = maNT;
        this.maTC = maTC;
        this.maCN = maCN;
    }

    public String getMaNL() {
        return maNL;
    }

    public void setMaNL(String maNL) {
        this.maNL = maNL;
    }

    public String getTenNL() {
        return tenNL;
    }

    public void setTenNL(String tenNL) {
        this.tenNL = tenNL;
    }

    public String getMaNT() {
        return maNT;
    }

    public void setMaNT(String maNT) {
        this.maNT = maNT;
    }

    public String getMaTC() {
        return maTC;
    }

    public void setMaTC(String maTC) {
        this.maTC = maTC;
    }

    public String getMaCN() {
        return maCN;
    }

    public void setMaCN(String maCN) {
        this.maCN = maCN;
    }

    public String formatDateString (Date date) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        String dateString = df.format(date);
        return dateString;
    }
    
    public Date formatStringDate (String dateString) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = df.parse(dateString);
        return date;
    }

}
