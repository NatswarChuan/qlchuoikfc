package DTO;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SanPham {
    private String maMonAn;
    private String tenMonAn;
    private String tgCheBien;
    private String maNH;
    private String maCN;
    private String kiemNghiem;

    public SanPham() {
    }

    public SanPham(String maMonAn, String tenMonAn, String tgCheBien, String maNH, String maCN, String kiemNghiem) {
        this.maMonAn = maMonAn;
        this.tenMonAn = tenMonAn;
        this.tgCheBien = tgCheBien;
        this.maNH = maNH;
        this.maCN = maCN;
        this.kiemNghiem = kiemNghiem;
    }

    public String getMaMonAn() {
        return maMonAn;
    }

    public void setMaMonAn(String maMonAn) {
        this.maMonAn = maMonAn;
    }

    public String getTenMonAn() {
        return tenMonAn;
    }

    public void setTenMonAn(String tenMonAn) {
        this.tenMonAn = tenMonAn;
    }

    public String getTgCheBien() {
        return tgCheBien;
    }

    public void setTgCheBien(String tgCheBien) {
        this.tgCheBien = tgCheBien;
    }

    public String getMaCN() {
        return maCN;
    }

    public void setMaCN(String maCN) {
        this.maCN = maCN;
    }

    public String getKiemNghiem() {
        return kiemNghiem;
    }

    public void setKiemNghiem(String kiemNghiem) {
        this.kiemNghiem = kiemNghiem;
    }

    public String getMaNH() {
        return maNH;
    }

    public void setMaNH(String maNH) {
        this.maNH = maNH;
    }
       
    public String formatDateString (Date date) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        String dateString = df.format(date);
        return dateString;
    }
    
    public Date formatStringDate (String dateString) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = df.parse(dateString);
        return date;
    }
}
