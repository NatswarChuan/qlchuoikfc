package DTO;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DaiLy {
    private String maNH;
    private String tenNH;
    private String hopDongMB;
    private String diaChi;
    private String SDT;
    private String maTK;
    private String tgNhanHang;

    public String getTgNhanHang() {
        return tgNhanHang;
    }

    public void setTgNhanHang(String tgNhanHang) {
        this.tgNhanHang = tgNhanHang;
    }
    
    public DaiLy() {
    }

    public DaiLy(String maNH, String tenNH, String hopDongMB, String diaChi, String SDT, String maTK) {
        this.maNH = maNH;
        this.tenNH = tenNH;
        this.hopDongMB = hopDongMB;
        this.diaChi = diaChi;
        this.SDT = SDT;
        this.maTK = maTK;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getMaNH() {
        return maNH;
    }

    public void setMaNH(String maNH) {
        this.maNH = maNH;
    }

    public String getTenNH() {
        return tenNH;
    }

    public void setTenNH(String tenNH) {
        this.tenNH = tenNH;
    }

    public String getHopDongMB() {
        return hopDongMB;
    }

    public void setHopDongMB(String hopDongMB) {
        this.hopDongMB = hopDongMB;
    }

    public String getMaTK() {
        return maTK;
    }

    public void setMaTK(String maTK) {
        this.maTK = maTK;
    }
     
    public String formatDateString (Date date) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        String dateString = df.format(date);
        return dateString;
    }
    
    public Date formatStringDate (String dateString) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = df.parse(dateString);
        return date;
    }
}
