package DTO;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NhapHangNPP {
    private String maPNPP;
    private String maNT;
    private String tgNhap;
    private String maNPP;

    public NhapHangNPP() {
    }

    public NhapHangNPP(String maPNPP, String maNT, String tgNhap, String maNPP) {
        this.maPNPP = maPNPP;
        this.maNT = maNT;
        this.tgNhap = tgNhap;
        this.maNPP = maNPP;
    }    

    public String getMaPNPP() {
        return maPNPP;
    }

    public void setMaPNPP(String maPNPP) {
        this.maPNPP = maPNPP;
    }

    public String getMaNT() {
        return maNT;
    }

    public void setMaNT(String maNT) {
        this.maNT = maNT;
    }

    public String getTgNhap() {
        return tgNhap;
    }

    public void setTgNhap(String tgNhap) {
        this.tgNhap = tgNhap;
    }

    public String getMaNPP() {
        return maNPP;
    }

    public void setMaNPP(String maNPP) {
        this.maNPP = maNPP;
    }
 
    public String formatDateString (Date date) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        String dateString = df.format(date);
        return dateString;
    }
    
    public Date formatStringDate (String dateString) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = df.parse(dateString);
        return date;
    }
}
