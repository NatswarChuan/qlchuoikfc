package DTO;

public class NguonGoc {
    private String maMonAn;
    private String maNL;

    public NguonGoc() {
    }

    public NguonGoc(String maMonAn, String maNL) {
        this.maMonAn = maMonAn;
        this.maNL = maNL;
    }

    public String getMaMonAn() {
        return maMonAn;
    }

    public void setMaMonAn(String maMonAn) {
        this.maMonAn = maMonAn;
    }

    public String getMaNL() {
        return maNL;
    }

    public void setMaNL(String maNL) {
        this.maNL = maNL;
    }
}
