package DTO;

public class ChiTietHoaDon {
    private String maHD;
    private String maMonAn;
    private int soLuong;

    public ChiTietHoaDon() {
    }

    public ChiTietHoaDon(String maHD, String maMonAn, int soLuong) {
        this.maHD = maHD;
        this.maMonAn = maMonAn;
        this.soLuong = soLuong;
    }

    public String getMaHD() {
        return maHD;
    }

    public void setMaHD(String maHD) {
        this.maHD = maHD;
    }

    public String getMaMonAn() {
        return maMonAn;
    }

    public void setMaMonAn(String maMonAn) {
        this.maMonAn = maMonAn;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }
    
}
