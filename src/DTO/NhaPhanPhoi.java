package DTO;

public class NhaPhanPhoi {
    private String maNPP;
    private String tenNPP;
    private String diaChi;
    private String SDT;
    private String maTK;

    public NhaPhanPhoi() {
    }

    public NhaPhanPhoi(String maNPP, String tenNPP,  String diaChi, String SDT, String maTK) {
        this.maNPP = maNPP;
        this.tenNPP = tenNPP;
        this.diaChi = diaChi;
        this.SDT = SDT;
        this.maTK = maTK;
    }

    public String getMaNPP() {
        return maNPP;
    }

    public void setMaNPP(String maNPP) {
        this.maNPP = maNPP;
    }

    public String getTenNPP() {
        return tenNPP;
    }

    public void setTenNPP(String tenNPP) {
        this.tenNPP = tenNPP;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getSDT() {
        return SDT;
    }

    public void setSDT(String SDT) {
        this.SDT = SDT;
    }

    public String getMaTK() {
        return maTK;
    }

    public void setMaTK(String maTK) {
        this.maTK = maTK;
    }
    
    
}
