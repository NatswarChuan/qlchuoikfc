package DTO;

public class ChungNhan {
    private String maCN;
    private String tenCN;
    private String kiemTra;

    public ChungNhan() {
    }

    public ChungNhan(String maCN, String tenCN, String kiemTra) {
        this.maCN = maCN;
        this.tenCN = tenCN;
        this.kiemTra = kiemTra;
    }
    
    public String getKiemTra() {
        return kiemTra;
    }

    public void setKiemTra(String kiemTra) {
        this.kiemTra = kiemTra;
    }

    public String getMaCN() {
        return maCN;
    }

    public void setMaCN(String maCN) {
        this.maCN = maCN;
    }

    public String getTenCN() {
        return tenCN;
    }

    public void setTenCN(String tenCN) {
        this.tenCN = tenCN;
    }
}
