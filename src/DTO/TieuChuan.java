package DTO;

public class TieuChuan {
    private String maTC;
    private String tenTC;
    private String kiemTra;

    public TieuChuan() {
    }

    public TieuChuan(String maTC, String tenTC, String kiemTra) {
        this.maTC = maTC;
        this.tenTC = tenTC;
        this.kiemTra = kiemTra;
    }

    public String getMaTC() {
        return maTC;
    }

    public void setMaTC(String maTC) {
        this.maTC = maTC;
    }

    public String getTenTC() {
        return tenTC;
    }

    public void setTenTC(String tenTC) {
        this.tenTC = tenTC;
    }

    public String getKiemTra() {
        return kiemTra;
    }

    public void setKiemTra(String kiemTra) {
        this.kiemTra = kiemTra;
    }
    
}
