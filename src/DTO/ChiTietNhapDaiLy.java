package DTO;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChiTietNhapDaiLy {
    private String maPNNH;
    private String maNL;
    private int soLuong;
    private String tgVanChuyen;

    public ChiTietNhapDaiLy() {
    }

    public ChiTietNhapDaiLy(String maPNNH, String maNL, int soLuong, String tgVanChuyen) {
        this.maPNNH = maPNNH;
        this.maNL = maNL;
        this.soLuong = soLuong;
        this.tgVanChuyen = tgVanChuyen;
    }

    public String getMaPNNH() {
        return maPNNH;
    }

    public void setMaPNNH(String maPNNH) {
        this.maPNNH = maPNNH;
    }

    public String getMaNL() {
        return maNL;
    }

    public void setMaNL(String maNL) {
        this.maNL = maNL;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public String getTgVanChuyen() {
        return tgVanChuyen;
    }

    public void setTgVanChuyen(String tgVanChuyen) {
        this.tgVanChuyen = tgVanChuyen;
    }
    
    public String formatDateString (Date date) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        String dateString = df.format(date);
        return dateString;
    }
    
    public Date formatStringDate (String dateString) throws ParseException {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date date = df.parse(dateString);
        return date;
    }
}
