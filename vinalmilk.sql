-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1:3306
-- Thời gian đã tạo: Th6 14, 2022 lúc 01:30 PM
-- Phiên bản máy phục vụ: 5.7.36
-- Phiên bản PHP: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `qlchuoinh`
--
CREATE DATABASE IF NOT EXISTS `qlchuoinh` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `qlchuoinh`;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chitiethoadon`
--

DROP TABLE IF EXISTS `chitiethoadon`;
CREATE TABLE IF NOT EXISTS `chitiethoadon` (
  `MA_HD` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MA_MON_AN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SO_LUONG` int(11) DEFAULT NULL,
  PRIMARY KEY (`MA_HD`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chitietnhap_nh`
--

DROP TABLE IF EXISTS `chitietnhap_nh`;
CREATE TABLE IF NOT EXISTS `chitietnhap_nh` (
  `MA_PNNH` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MA_NL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SO_LUONG` int(11) DEFAULT NULL,
  `THOI_GIAN_VAN_CHUYEN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_PNNH`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chitietnhap_npp`
--

DROP TABLE IF EXISTS `chitietnhap_npp`;
CREATE TABLE IF NOT EXISTS `chitietnhap_npp` (
  `MA_PNPP` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MA_NL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SO_LUONG` int(11) DEFAULT NULL,
  `THOI_GIAN_VAN_CHUYEN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_PNPP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chitietnhap_npp`
--

INSERT INTO `chitietnhap_npp` (`MA_PNPP`, `MA_NL`, `SO_LUONG`, `THOI_GIAN_VAN_CHUYEN`) VALUES
('tien', 'tien', 0, '2022-06-12'),
('23123', 'tien', 0, '2022-06-12'),
('ac', 'ST', 0, '2022-06-12'),
('fghjkl', 'ST', 0, '2022-06-14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chungnhan`
--

DROP TABLE IF EXISTS `chungnhan`;
CREATE TABLE IF NOT EXISTS `chungnhan` (
  `MA_CN` varchar(129) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `TEN_CN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `KIEM_TRA` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_CN`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chungnhan`
--

INSERT INTO `chungnhan` (`MA_CN`, `TEN_CN`, `KIEM_TRA`) VALUES
('lince', 'tiencn', 'cnroi'),
('ISO 22000:2011', 'ISO 22000:2011', NULL),
('ISO 9001:2008', 'ISO 9001:2008', NULL),
('ISO 140001:2004', 'ISO 140001:2004', NULL),
('ISO/17025:2005', 'ISO/17025:2005', NULL),
('HALAL', 'HALAL', NULL),
('VSATTP', 'VSATTP', NULL),
('ISO 22000:2011\r\nISO 9001:2008\r\nISO 140001:2004\r\nISO/17025:2005\r\nHALAL\r\nVSATTP\r\n', 'ISO 22000:2011\r\nISO 9001:2008\r\nISO 140001:2004\r\nISO/17025:2005\r\nHALAL\r\nVSATTP\r\n', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hoadon`
--

DROP TABLE IF EXISTS `hoadon`;
CREATE TABLE IF NOT EXISTS `hoadon` (
  `MA_HD` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NGAY_XUAT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TEN_KH` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_NH` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `monan`
--

DROP TABLE IF EXISTS `monan`;
CREATE TABLE IF NOT EXISTS `monan` (
  `MA_MON_AN` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TEN_MON_AN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TG_CHE_BIEN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_NH` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_CN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `KIEM_NGHIEM` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_MON_AN`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `monan`
--

INSERT INTO `monan` (`MA_MON_AN`, `TEN_MON_AN`, `TG_CHE_BIEN`, `MA_NH`, `MA_CN`, `KIEM_NGHIEM`) VALUES
('STVSDD', 'Sữa tươi và sữa dinh dưỡng', NULL, NULL, NULL, NULL),
('SC', 'Sữa chua', NULL, NULL, NULL, NULL),
('STV', 'Sữa thực vật', NULL, NULL, NULL, NULL),
('SGK', 'Nước giải khát', NULL, NULL, NULL, NULL),
('K', 'Kem', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nguongoc`
--

DROP TABLE IF EXISTS `nguongoc`;
CREATE TABLE IF NOT EXISTS `nguongoc` (
  `MA_MON_AN` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MA_NL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_MON_AN`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nguongoc`
--

INSERT INTO `nguongoc` (`MA_MON_AN`, `MA_NL`) VALUES
('NG1', 'Mỹ'),
('NG2', 'Úc'),
('NG3', 'New Zealand'),
('NG4', 'EU '),
('NG5', 'Nhật Bản');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nguyenlieu`
--

DROP TABLE IF EXISTS `nguyenlieu`;
CREATE TABLE IF NOT EXISTS `nguyenlieu` (
  `MA_NL` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TEN_NL` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_NT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_TC` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_CN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_NL`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nguyenlieu`
--

INSERT INTO `nguyenlieu` (`MA_NL`, `TEN_NL`, `MA_NT`, `MA_TC`, `MA_CN`) VALUES
('ST', 'sữa tươi', 'nt1', 'Không biến đổi gene', 'ISO 22000:2011'),
('SDCD', 'sữa đặc có đường', 'nt1', 'Không biến đổi gene', 'ISO 22000:2011'),
('MUOI', 'muối', 'nt1', 'Không biến đổi gene', 'ISO 22000:2011'),
('HV', 'hương vị', 'nt1', 'Không biến đổi gene', 'ISO 22000:2011'),
('MEN', 'men', 'nt1', 'Không biến đổi gene', 'ISO 22000:2011'),
('HDN', 'Hạt đậu nành', 'nt1', 'Không sử dụng thuốc trừ sâu', 'ISO 22000:2011'),
('HOC', 'hạt óc chó', 'nt1', 'Không sử dụng thuốc trừ sâu', 'ISO 22000:2011'),
('TC', 'Trái cây', 'nt1', 'Không sử dụng thuốc trừ sâu', 'ISO 22000:2011'),
('TA', 'trà atiso', 'nt1', 'Không sử dụng thuốc trừ sâu', 'ISO 22000:2011'),
('NTK', 'nước tinh khiết Icy Prenium', 'nt1', 'Không sử dụng thuốc trừ sâu', 'ISO 22000:2011'),
('TG', 'trứng gà', 'nt1', 'Không biến đổi gene', 'ISO 22000:2011'),
('SD', 'sữa đặc', 'nt1', 'Không biến đổi gene', 'ISO 22000:2011'),
('HUONG', 'hương', 'nt1', 'Không sử dụng thuốc trừ sâu', 'ISO 22000:2011');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhahang`
--

DROP TABLE IF EXISTS `nhahang`;
CREATE TABLE IF NOT EXISTS `nhahang` (
  `MA_NH` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TEN_NH` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `HOP_DONG_MUA_BAN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `TG_NHAP_HANG` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DIA_CHI` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SDT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_TK` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_NH`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nhahang`
--

INSERT INTO `nhahang` (`MA_NH`, `TEN_NH`, `HOP_DONG_MUA_BAN`, `TG_NHAP_HANG`, `DIA_CHI`, `SDT`, `MA_TK`) VALUES
('BL1', '212.000 điểm bán lẻ', NULL, NULL, 'Số 8 đường D1, KP1, P. Linh Tây, TP. Thủ Đức, TP. Hồ Chí Minh', '0973337725', 'nguyenhoangthuanphat123'),
('BL2', '1.500 siêu thị lớn nhỏ ', NULL, NULL, 'Số 119 Dân Chủ, P. Bình Thọ, Q. Thủ Đức, TP. Hồ Chí Minh', '01213448582', 'nguyenhoangthuanphat123'),
('BL3', '600 cửa hàng tiện lợi', NULL, NULL, 'Số 223 Nguyễn Công Trứ, P. Nguyễn Thái Bình, Q. 1, TP. Hồ Chí Minh', '01999401029', 'nguyenhoangthuanphat123');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhaphang_nh`
--

DROP TABLE IF EXISTS `nhaphang_nh`;
CREATE TABLE IF NOT EXISTS `nhaphang_nh` (
  `MA_PNNH` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MA_NPP` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `THOI_GIAN_NHAP` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_NH` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_PNNH`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhaphang_npp`
--

DROP TABLE IF EXISTS `nhaphang_npp`;
CREATE TABLE IF NOT EXISTS `nhaphang_npp` (
  `MA_PNPP` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `MA_NT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `THOI_GIAN_NHAP` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_NPP` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_PNPP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nhaphang_npp`
--

INSERT INTO `nhaphang_npp` (`MA_PNPP`, `MA_NT`, `THOI_GIAN_NHAP`, `MA_NPP`) VALUES
('ac', 'nt2', '2022-06-12', 'nguyenhoangthuanphat123');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhaphanphoi`
--

DROP TABLE IF EXISTS `nhaphanphoi`;
CREATE TABLE IF NOT EXISTS `nhaphanphoi` (
  `MA_NPP` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TEN_NPP` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `HOP_DONG_MUA_BAN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DIA_CHI` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SDT` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_TK` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_NPP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nhaphanphoi`
--

INSERT INTO `nhaphanphoi` (`MA_NPP`, `TEN_NPP`, `HOP_DONG_MUA_BAN`, `DIA_CHI`, `SDT`, `MA_TK`) VALUES
('npp2', 'nguyenhoangthuanphat123', NULL, 'Số 119 Dân Chủ, P. Bình Thọ, Q. Thủ Đức, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123'),
('npp1', 'nguyenhoangthuanphat123', NULL, 'Số 8 đường D1, KP1, P. Linh Tây, TP. Thủ Đức, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123'),
('npp3', 'nguyenhoangthuanphat123', NULL, 'Số 47 Hiệp Bình, P. Hiệp Bình Chánh, Q. Thủ Đức, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123'),
('npp4', 'nguyenhoangthuanphat123', NULL, 'Số 89 Cách Mạng Tháng Tám, P. Bến Thành, Q. 1, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123'),
('npp5', 'nguyenhoangthuanphat123', NULL, 'Số 119 Dân Chủ, P. Bình Thọ, Q. Thủ Đức, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123'),
('npp6', 'nguyenhoangthuanphat123', NULL, 'Số 308 Nguyễn Thượng Hiền, P. 4, Q. 3, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123'),
('npp7', 'nguyenhoangthuanphat123', NULL, 'Số 190 Nguyễn Đình Chiểu, P. 6, Q. 3, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123'),
('npp8', 'nguyenhoangthuanphat123', NULL, 'Số 177 Minh Phụng, P. 9, Q. 6, TP. Hồ Chí Minh.', '0908987712', 'nguyenhoangthuanphat123'),
('npp9', 'nguyenhoangthuanphat123', NULL, 'Số 473 Nguyễn Văn Luông, P. 12, Q. 6, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123'),
('npp10', 'nguyenhoangthuanphat123', NULL, 'Số 133 Chu Văn An, P. 26, Q. Bình Thạnh, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123'),
('npp11', 'nguyenhoangthuanphat123', NULL, 'Số 482 Lê Quang Định, P. 11, Q. Bình Thạnh, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123'),
('npp12', 'nguyenhoangthuanphat123', NULL, 'Số 202-204 Lê Quang Định, P. 14, Q. Bình Thạnh, TP. Hồ Chí Minh', '0908987712', 'nguyenhoangthuanphat123');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nongtrai`
--

DROP TABLE IF EXISTS `nongtrai`;
CREATE TABLE IF NOT EXISTS `nongtrai` (
  `MA_NT` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TEN_NT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `CHU_SO_HUU` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `DIA_CHI` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `SDT` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `MA_TK` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_NT`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nongtrai`
--

INSERT INTO `nongtrai` (`MA_NT`, `TEN_NT`, `CHU_SO_HUU`, `DIA_CHI`, `SDT`, `MA_TK`) VALUES
('nt1', 'Trang trại Vinamilk Organic Đà Lạt', 'chauphucnguyen551', 'Số 47 Hiệp Bình, P. Hiệp Bình Chánh, Q. Thủ Đức, TP. Hồ Chí Minh', '0908987712', 'chauphucnguyen551');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `taikhoan`
--

DROP TABLE IF EXISTS `taikhoan`;
CREATE TABLE IF NOT EXISTS `taikhoan` (
  `MA_TK` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PASSWORD` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `VAI_TRO` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_TK`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `taikhoan`
--

INSERT INTO `taikhoan` (`MA_TK`, `PASSWORD`, `VAI_TRO`) VALUES
('nguyenhoangthuanphat123', '123456', 'Nhà phân phối'),
('chauphucnguyen551', '123456', 'Nông trại'),
('Thuytrang@123', '123456', 'ADMIN');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tieuchuan`
--

DROP TABLE IF EXISTS `tieuchuan`;
CREATE TABLE IF NOT EXISTS `tieuchuan` (
  `MA_TC` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `TEN_TC` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `KIEM_TRA` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MA_TC`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tieuchuan`
--

INSERT INTO `tieuchuan` (`MA_TC`, `TEN_TC`, `KIEM_TRA`) VALUES
('Không biến đổi gene', 'Không biến đổi gene', NULL),
('Không sử dụng hormone tăng trưởng', 'Không sử dụng hormone tăng trưởng', NULL),
('Không sử dụng thuốc kháng sinh', 'Không sử dụng thuốc kháng sinh', NULL),
('Không sử dụng thuốc trừ sâu', 'thuốc trừ sâu', NULL),
('Không phân bón hóa học', 'phân bón hóa học', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
